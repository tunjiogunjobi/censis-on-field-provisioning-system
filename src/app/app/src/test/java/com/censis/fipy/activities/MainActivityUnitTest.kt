package com.censis.fipy.activities;

import com.censis.fipy.R
import com.censis.fipy.presentation.ui.activities.kotlin.MainActivity
import com.censis.fipy.presentation.ui.fragments.ScanDevicesFragment
import com.google.common.truth.Truth.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.Shadows

/**
 *
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(RobolectricTestRunner::class)
class MainActivityUnitTest {

    lateinit var mainActivity: MainActivity

    @Before fun setUp(){
        mainActivity = Robolectric.setupActivity(MainActivity::class.java)

    }

    /**
     * A quick test to ensure our activity is not null before other test is executed.
     */
    @Test fun activityShouldNotBeNull(){
        assertThat(mainActivity).isNotNull()
    }

    @Test fun shouldDisplayToolbar(){
        val toolbar:androidx.appcompat.widget.Toolbar = Shadows.shadowOf(mainActivity).contentView.findViewById(R.id.toolbar)

        assertThat(toolbar).isNotNull();
    }


   @Test fun shouldDisplayHomeFragmentOnStartUp(){
       val homeFragment: ScanDevicesFragment = Robolectric.buildFragment(ScanDevicesFragment::class.java)
    }

    @After fun tearDown(){
        mainActivity.finish()
    }


}