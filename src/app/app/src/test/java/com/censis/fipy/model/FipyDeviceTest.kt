package com.censis.fipy.model.kotlin

import com.censis.fipy.model.FipyDevice
import com.google.common.truth.Truth.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test

class FipyDeviceTest {

    val expectedDeviceName: String = "Censis"
    val expectedDeviceAddress: String = "4A:A1:C6:2F:3E:7F"
    var expectedAppEui: String = "70B3D57ED00169B8"
    var expectedAppKey: String = ""

    lateinit var device: FipyDevice


    @Before
    fun setUp() {
        device = FipyDevice("Censis","70B3D57ED00169B8","4A:A1:C6:2F:3E:7F", "")
    }

    @After
    fun tearDown() {
    }

    @Test
    fun testDeviceDetails(){
        assertThat(expectedDeviceName).isEqualTo(device.dev_id)
        assertThat(expectedDeviceAddress).isEqualTo(device.bleMacAddress)
        assertThat(expectedAppEui).isEqualTo(device.serviceUUID)
        assertThat(expectedAppKey).isEqualTo(device.appKey)
        //assertforeacht of the properties

    }
}