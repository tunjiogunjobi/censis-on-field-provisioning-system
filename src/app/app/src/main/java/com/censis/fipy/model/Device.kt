package com.censis.fipy.model

import java.io.Serializable
import java.time.Instant

abstract class Device : Serializable {
    // TODO: define devices implementation
    abstract val devId: String
    abstract val appId: String
    abstract val description: String
    abstract val position: Coordinates?
    abstract val altitude: Int
    abstract val lastSeen: Instant?

    abstract val appEui: ByteArray
    abstract val devEui: ByteArray
    abstract val appKey: ByteArray

    abstract val devAddress: ByteArray

    abstract val appSessionKey: ByteArray
    abstract val networkSessionKey: ByteArray


}