package com.censis.fipy.network.api.thethingsnetwork.applications

import androidx.annotation.VisibleForTesting
import io.grpc.ClientInterceptor
import io.grpc.MethodDescriptor
import io.grpc.CallOptions
import io.grpc.Channel
import io.grpc.ClientCall
import io.grpc.ForwardingClientCall.SimpleForwardingClientCall
import io.grpc.ForwardingClientCallListener.SimpleForwardingClientCallListener
import io.grpc.Metadata

internal class Interceptor (private val getBearerToken: () -> String) : ClientInterceptor {

    override fun <ReqT, RespT> interceptCall(method: MethodDescriptor<ReqT, RespT>,
                                             callOptions: CallOptions,
                                             next: Channel): ClientCall<ReqT, RespT> {
        val self = this
        return object : SimpleForwardingClientCall<ReqT, RespT>(next.newCall(method, callOptions)) {
            override fun start(responseListener: ClientCall.Listener<RespT>, headers: Metadata) {
                headers.put(KEY, self.getBearerToken())
                super.start(object : SimpleForwardingClientCallListener<RespT>(responseListener) {}, headers)
            }
        }
    }

    companion object {
        @VisibleForTesting
        private val KEY = Metadata.Key.of("token", Metadata.ASCII_STRING_MARSHALLER)
    }
}
