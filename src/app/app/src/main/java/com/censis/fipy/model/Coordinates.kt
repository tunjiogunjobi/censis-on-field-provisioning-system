package com.censis.fipy.model

data class Coordinates (
        val latitude: Float,
        val longitude: Float
)