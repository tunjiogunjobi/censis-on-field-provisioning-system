package com.censis.fipy.model;

import java.util.List;

import androidx.lifecycle.MutableLiveData;


public class DeviceRepository {

    private MutableLiveData<List<FipyDevice>> mScanResult = new MutableLiveData<>();
    private static DeviceRepository mInstance;


    /**
     *
     * @return instance of a Device Repository
     */


    public static DeviceRepository getInstance(){

       if(mInstance == null){
            synchronized (DeviceRepository.class) {
                mInstance = new DeviceRepository();
            }
      }
            return mInstance;
        }

    public MutableLiveData<List<FipyDevice>> getMutableLiveData(){
        return mScanResult;
    }


    private List<FipyDevice> getDeviceList(){
        return mScanResult.getValue();
    }


    private FipyDevice getDevice(int position){
        return getDeviceList().get(position);
    }

    public List<String> getGattServices(FipyDevice device){
        return device.getGattServices();
    }


}
