package com.censis.fipy

import android.util.Base64
import java.io.*

object Utils {
    fun hexToBytes(hexs: String): ByteArray {
        if (hexs.length % 2 == 0) {
            var bytes = ByteArray(hexs.length/2)
            for (i in 0 until hexs.length/2) {
                val idx = i*2
                bytes[i] = Integer.parseInt(hexs.substring(idx, idx+2), 16).toByte()
            }
            return bytes
        }

        throw Exception("Invalid bytes")
    }

    @JvmStatic fun bytesToHex(bytes: ByteArray, separator: String = ""): String {
        return bytes.joinToString(separator) { String.format("%02X", it) }
    }

    fun <T> serializeObject(obj: T): String {
        val bytesStream = ByteArrayOutputStream()
        val objStream = ObjectOutputStream(bytesStream)
        objStream.writeObject(obj)
        objStream.close()
        return Base64.encodeToString(bytesStream.toByteArray(), 0)
    }

    fun <T> unserializeObject(obj: String): T {
        val bytesStream = ByteArrayInputStream(Base64.decode(obj, 0))
        val objStream = ObjectInputStream(bytesStream)
        return objStream.readObject() as T
    }
}