package com.censis.fipy.presentation.adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.censis.fipy.R;
import com.censis.fipy.model.Application;
import com.censis.fipy.model.FipyDevice;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

public class ScanListAdapter extends RecyclerView.Adapter<ScanListAdapter.CustomViewHolder> implements DeviceSelectedCallBack{
    private List<FipyDevice> mScanDetails;
    private final String FIPY_DEVICE = "fipyDevice";

    private Bundle mBundle;



    public ScanListAdapter(){
        mBundle = new Bundle();

    }

    public void addDevice(int position) {


    }

    @Override
    public void setApplication(Application app) {
        mBundle.putSerializable("app",app);
    }


    public static class CustomViewHolder extends RecyclerView.ViewHolder{
        public TextView mTextViewDeviceName, mTextviewAddress;

        public CustomViewHolder(final View view){
            super(view);
            mTextViewDeviceName = view.findViewById(R.id.textView_device_name);
            mTextviewAddress = view.findViewById(R.id.textView_device_address);

        }
    }




    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.recyclerview_item,parent,false);

        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        if (mScanDetails != null) {
            final FipyDevice device = mScanDetails.get(position);
            String name = device.getDev_id();
            String address = device.getBleMacAddress();
            if(name == null) name = "Unknown";
            holder.mTextViewDeviceName.setText(name);
            holder.mTextviewAddress.setText(address);
            final View view = holder.mTextViewDeviceName.getRootView();
            view.setOnClickListener(v -> {
                mBundle.putString("bleMacAddress",device.getBleMacAddress());
                mBundle.putSerializable(FIPY_DEVICE,device);
                Navigation.findNavController(view).navigate(R.id.nav_deviceRegistrationFragment,mBundle);
            });


        } else {
            // Covers the case of data not being ready yet.
            holder.mTextViewDeviceName.setText("No Device Found");
        }
    }



    @Override
    public int getItemCount() {
        if (mScanDetails != null)
            return mScanDetails.size();
        else return 0;

    }



    public void setDevices(List<FipyDevice> devices){
        mScanDetails = devices;
        notifyDataSetChanged();

    }

    void clear(){
        if(mScanDetails != null)
            mScanDetails.clear();
    }




}
