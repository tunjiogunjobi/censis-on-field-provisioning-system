package com.censis.fipy.presentation.ui.fragments.kotlin

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.censis.fipy.R
import com.censis.fipy.Utils
import com.censis.fipy.model.Application
import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import com.censis.fipy.model.Device
import com.censis.fipy.network.api.UserUnauthenticatedException
import com.censis.fipy.presentation.ui.activities.kotlin.MainActivity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.util.Log
import android.view.MenuItem
import android.widget.*
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.fragment_device_details.*
import org.jetbrains.anko.*
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*


class DeviceDetailsFragment : Fragment() {

    companion object {
        const val BLEMACADDR_REGEX = "bleMacAddress:([0-9a-fA-F]+)"
    }

    lateinit var app: Application
    lateinit var devId: String
    var device: Device? = null

    lateinit var pullToRefresh: SwipeRefreshLayout
    lateinit var content: ScrollView

    var bleMacAddress: ByteArray? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
        arguments?.let{
            app = it.getSerializable("app") as Application
            devId = it.getString("devId", "")
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(
                R.layout.fragment_device_details,
                container,
                false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        content = view.findViewById(R.id.deviceDetailsContent)

        pullToRefresh = view.findViewById(R.id.pullToRefreshDevice)
        pullToRefresh.setOnRefreshListener {
            loadDevice()
        }

        view.findViewById<ImageButton>(R.id.openInMap)
                .setOnClickListener {
                    val uri = String.format("geo:%f,%f?q=%f,%f(%s)",
                            device?.position!!.latitude,
                            device?.position!!.longitude,
                            device?.position!!.latitude,
                            device?.position!!.longitude,
                            devId)
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                    context?.startActivity(intent)
                }

        attachCopyToClipboardListener<ImageButton>(
                R.id.copyAppEui,
                "App EUI"
        ) { Utils.bytesToHex(device!!.appEui) }

        attachCopyToClipboardListener<ImageButton>(
                R.id.copyDevEui,
                "Dev EUI"
        ) { Utils.bytesToHex(device!!.devEui) }

        attachCopyToClipboardListener<ImageButton>(
                R.id.copyAppKey,
                "App key"
        ) { Utils.bytesToHex(device!!.appKey) }

        attachCopyToClipboardListener<ImageButton>(
                R.id.copyDevAddr,
                "Device address"
        ) { Utils.bytesToHex(device!!.devAddress) }

        attachCopyToClipboardListener<ImageButton>(
                R.id.copyNetSKey,
                "Network session key"
        ) { Utils.bytesToHex(device!!.networkSessionKey) }

        attachCopyToClipboardListener<ImageButton>(
                R.id.copyAppSKey,
                "App session key"
        ) { Utils.bytesToHex(device!!.appSessionKey) }

        view.findViewById<Button>(R.id.deleteDevice)
                .setOnClickListener {
                    context?.alert(R.string.deleteDeviceConfirmation)
                    {
                        yesButton { deleteDevice() }
                        noButton {}
                    }?.show()
                }
    }

    private fun <T : View> attachCopyToClipboardListener(el: Int, label: String, value: () -> String) {
        view?.findViewById<T>(el)?.
                setOnClickListener {
                    val clipboard = context?.
                            getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
                    clipboard?.let {
                        it.primaryClip = ClipData.newPlainText(label, value())
                        context?.toast(
                                resources.getString(
                                        R.string.copiedToClipboard,
                                        label
                                )
                        )
                    }
                }
    }

    private fun loadDevice() {
        val mainActivity = activity as MainActivity

        pullToRefresh.isRefreshing = true
        content.visibility = View.GONE
        view?.findViewById<TextView>(R.id.deviceError)?.visibility = View.GONE
        AsyncTask.execute {
            try {
                mainActivity.apiService?.getDevice(app, devId)?.let {
                    device = it
                }

                activity?.runOnUiThread {
                    setDeviceVariables()
                    content.visibility = View.VISIBLE
                }
            } catch(e: UserUnauthenticatedException) {
                activity?.runOnUiThread {
                    context!!.toast(R.string.user_unauthenticated)
                }
                mainActivity.resetAndGoToLogin(NavHostFragment.findNavController(this))
                TODO("recover on timeout or no network")
            } catch(e: Exception) {
                activity?.runOnUiThread {
                    view?.findViewById<TextView>(R.id.deviceError)?.visibility = View.VISIBLE
                    content.visibility = View.GONE
                }
            }

            activity?.runOnUiThread {
                pullToRefresh.isRefreshing = false
            }
        }
    }

    private fun setDeviceVariables() {


        view?.findViewById<TextInputEditText>(R.id.appEui)?.setText(
                Utils.bytesToHex(device!!.appEui, " ")
        )

        view?.findViewById<TextInputEditText>(R.id.devEui)?.setText(
                Utils.bytesToHex(device!!.devEui, " ")
        )

        view?.findViewById<TextInputEditText>(R.id.appKey)?.setText(
                Utils.bytesToHex(device!!.appKey, " ")
        )

        view?.findViewById<TextInputEditText>(R.id.devAddr)?.setText(
                Utils.bytesToHex(device!!.devAddress, " ")
        )

        view?.findViewById<TextInputEditText>(R.id.appSKey)?.setText(
                Utils.bytesToHex(device!!.appSessionKey, " ")
        )

        view?.findViewById<TextInputEditText>(R.id.netSKey)?.setText(
                Utils.bytesToHex(device!!.networkSessionKey, " ")
        )

        device!!.position?.let {
            view?.findViewById<TextInputEditText>(R.id.deviceLatitude)?.setText(
                    it.latitude.toString()
            )
            view?.findViewById<TextInputEditText>(R.id.deviceLongitude)?.setText(
                    it.longitude.toString()
            )
            view?.findViewById<TextView>(R.id.coordinatesUnknown)?.visibility = View.GONE
            view?.findViewById<LinearLayout>(R.id.deviceCoordinates)?.visibility = View.VISIBLE
        }

        device!!.lastSeen?.let {
            val lastSeen = view?.findViewById<TextView>(R.id.deviceLastSeen)
            val formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
                    .withLocale(getCurrentLocale(context!!))
                    .withZone(ZoneId.systemDefault())
            lastSeen?.text = formatter.format(it)
            lastSeen?.setTypeface(lastSeen.typeface)
        }
    }

    // https://stackoverflow.com/questions/14389349/android-get-current-locale-not-default
    fun getCurrentLocale(context: Context): Locale {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.resources.configuration.locales.get(0)
        } else {

            context.resources.configuration.locale
        }
    }


    private fun deleteDevice() {
        val mainActivity = activity as MainActivity
        mainActivity.apiService?.let {
            try {
                it.deleteDevice(app, devId)
                NavHostFragment.findNavController(this).navigateUp()
                context?.toast(R.string.deviceDeleted)
            } catch(e: Exception) {
                Log.d("dev", "deletion failed", e)
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val mainActivity = activity as MainActivity
        mainActivity.title = devId
        mainActivity.supportActionBar!!.setDisplayUseLogoEnabled(false)
        mainActivity.supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onResume() {
        super.onResume()

        if(device == null)
            loadDevice()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        Log.d("dev", "selecting $item.itemId")
        when (item.itemId) {
            android.R.id.home -> {
                Log.d("dev", "going up")
                NavHostFragment.findNavController(this).navigateUp()
                return true
            }
        }

        return false
    }
}