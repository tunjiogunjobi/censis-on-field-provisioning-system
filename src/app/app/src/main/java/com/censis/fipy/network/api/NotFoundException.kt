
package com.censis.fipy.network.api

class NotFoundException : Exception {
    constructor(message: String) : super(message)
    constructor(): super()
}