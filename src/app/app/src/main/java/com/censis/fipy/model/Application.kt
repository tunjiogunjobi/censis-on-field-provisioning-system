package com.censis.fipy.model

import java.io.Serializable

abstract class Application : Serializable {
        abstract val id: String
        abstract val name: String
        abstract val euis: List<ByteArray>
}