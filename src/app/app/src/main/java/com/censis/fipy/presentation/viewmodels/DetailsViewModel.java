package com.censis.fipy.presentation.viewmodels;

import android.app.Application;


import com.censis.fipy.model.DeviceRepository;
import com.censis.fipy.model.FipyDevice;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;



public class DetailsViewModel extends AndroidViewModel {
    //private FipyBleServiceConnection mBleServiceConnection;
    private DeviceRepository mRepository;

    public DetailsViewModel(@NonNull Application application) {
        super(application);
        //mBleServiceConnection = new FipyBleServiceConnection(application.getApplicationContext());
        mRepository = DeviceRepository.getInstance();
    }

    public List<String> getGattServices(FipyDevice device) {
        //return (mBleServiceConnection == null)?null: mBleServiceConnection.getGattServices();
        return mRepository.getGattServices(device);
    }

//    public BroadcastReceiver getReceiver() {
//        return mBleServiceConnection;
//    }
//
//    public void connect(String deviceAddress){
//
//        if (mBleServiceConnection != null) {
//            mBleServiceConnection.connectToDevice(deviceAddress);
//
//            final boolean result = mBleServiceConnection.connectToDevice(deviceAddress);
//            Log.d(TAG, "Connect request result=" + result);
//        }
//    }
//
//    public void disconnect(){
//        mBleServiceConnection.disconnect();
//    }
//
//    public void unbindService() {
//        mBleServiceConnection.unBindBleService();
//        //mBleServiceConnection = null;
//
//    }
//
//    public boolean isConnected(){
//
//        return mBleServiceConnection.isConnected();
//    }
}
