package com.censis.fipy.network.ble;

public interface BleConnector {

    boolean connnectToFipyDevice();

    void disconnectFromFipyDevice();
}
