package com.censis.fipy.network.api.thethingsnetwork.applications

import java.io.ByteArrayInputStream
import java.io.IOException
import java.io.InputStream
import java.lang.AssertionError
import java.lang.Exception
import java.security.GeneralSecurityException
import java.security.KeyStore
import java.security.cert.CertificateFactory
import javax.net.ssl.*

class SslSocketFactory(val protocol: String) {

    companion object {
        private val PASSWORD = "".toCharArray()
    }

    fun trustCertificate(cert: String): SSLSocketFactory {
        val sslContext = SSLContext.getInstance(protocol)
        sslContext.init(null, makeTrustManagerFromCert(cert), null)
        return sslContext.socketFactory
    }

    private fun makeTrustManagerFromCert(crt: String): Array<TrustManager> {
        val certificate = CertificateFactory.getInstance("X.509")
                .generateCertificate(ByteArrayInputStream(crt.toByteArray()))

        val keyStore = KeyStore.getInstance(KeyStore.getDefaultType())
        keyStore.load(null, PASSWORD) // empty key store
        keyStore.setCertificateEntry("appCert", certificate)

        val keyManagerFactory = KeyManagerFactory.getInstance(
                KeyManagerFactory.getDefaultAlgorithm())
        keyManagerFactory.init(keyStore, PASSWORD)
        val trustManagerFactory = TrustManagerFactory.getInstance(
                TrustManagerFactory.getDefaultAlgorithm())
        trustManagerFactory.init(keyStore)

        val trustManagers = trustManagerFactory.trustManagers
        if(trustManagers.isNotEmpty() && trustManagers[0] is X509TrustManager)
            return trustManagers

        throw AssertionError("trust manager generation from custom certificate failed")
    }
}