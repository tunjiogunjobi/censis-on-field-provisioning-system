package com.censis.fipy.presentation.ui.view_model.kotlin

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.censis.fipy.model.FipyDevice

class SharedViewModel : ViewModel() {
    val selected = MutableLiveData<FipyDevice>()

    fun select(device: FipyDevice) {
        selected.value = device
    }
}