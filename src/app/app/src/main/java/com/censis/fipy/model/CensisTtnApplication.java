package com.censis.fipy.model;

import org.thethingsnetwork.account.sync.Application;

import java.io.Serializable;

public class CensisTtnApplication extends Application implements Serializable {
    /**
     * Create a new application
     *
     * @param _id   the new application ID
     * @param _name the new application name
     */
    public CensisTtnApplication(String _id, String _name) {
        super(_id, _name);
    }
}
