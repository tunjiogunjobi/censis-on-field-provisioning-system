package com.censis.fipy.network.api

import android.net.Uri
import com.censis.fipy.model.Application
import com.censis.fipy.model.Coordinates
import com.censis.fipy.model.Device
import com.censis.fipy.network.api.thethingsnetwork.applications.Models
import kotlin.collections.List

abstract class Service {
    abstract val isAuthenticated: Boolean
    abstract val serviceName: String

    abstract fun buildAuthenticationUri(redirectUri: Uri): Uri
    abstract fun authenticate(code: String): Boolean
    abstract fun logout(): Boolean

    abstract fun getUsername(): String
    abstract fun getEmailAddress(): String

    abstract fun getApplications(): List<Application>?
    abstract fun getApplication(appId: String): Application?
    abstract fun getDevices(app: Application): List<Device>?
    abstract fun getDevice(app: Application, id: String): Device?
    abstract fun addDevice(app: Application,
                           id: String,
                           devEui: ByteArray,
                           appEui: ByteArray? = null,
                           description: String = "",
                           coordinates: Coordinates = Coordinates(0f, 0f),
                           altitude: Int = 0,
                           activationConstraint: String = "otaa",
                           devAddress: ByteArray? = null,
                           networkSessionKey: ByteArray? = null,
                           appSessionKey: ByteArray? = null,
                           appKey: ByteArray? = null
    ): ByteArray
    abstract fun deleteDevice(app: Application, id: String)
}