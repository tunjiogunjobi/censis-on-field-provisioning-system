package com.censis.fipy.network.api.thethingsnetwork.applications

import android.annotation.SuppressLint
import android.util.Log
import com.censis.fipy.network.api.AlreadyExistsException
import com.censis.fipy.network.api.NotFoundException
import com.censis.fipy.network.api.UserUnauthenticatedException
import com.censis.fipy.network.api.UserUnauthorizedException
import com.google.protobuf.ByteString
import io.grpc.*
import io.grpc.okhttp.OkHttpChannelBuilder
import org.thethingsnetwork.api.handler.ApplicationIdentifier
import org.thethingsnetwork.api.handler.ApplicationManagerGrpc
import org.thethingsnetwork.api.handler.Device
import org.thethingsnetwork.api.handler.DeviceIdentifier
import java.util.concurrent.TimeUnit
import org.thethingsnetwork.api.protocol.lorawan.Device as LorawanDevice

class Handler(private val grpcUri: String,
              private val cert: String,
              private val getBearerToken: () -> String) {

    private lateinit var managedChannel: ManagedChannel
    private lateinit var blockingStub: ApplicationManagerGrpc.ApplicationManagerBlockingStub
    private var init: Boolean = false

    fun shutdown() {
        managedChannel.shutdownNow().awaitTermination(5, TimeUnit.SECONDS)
    }

    private fun init() {
        if(!init) {
            val grpcUri_ = grpcUri.split(":")
            managedChannel = OkHttpChannelBuilder.forAddress(grpcUri_[0], grpcUri_[1].toInt())
                    .sslSocketFactory(SslSocketFactory("TLS").trustCertificate(cert))
                    .intercept(Interceptor(getBearerToken))
                    .build()
            blockingStub = ApplicationManagerGrpc.newBlockingStub(managedChannel)
            Log.d("ttn.apps.handler", "$grpcUri")
            init = true
        }
    }

    fun getDevices(appId: String): List<Models.Device> {
        init()
        val req = ApplicationIdentifier.newBuilder().setAppId(appId).build()
        val devices = mutableListOf<Models.Device>()

        try {
            val devs = blockingStub.getDevicesForApplication(req)

            for(device in devs.devicesList)
                devices.add(Models.Device(device))

            return devices
        } catch (e: StatusRuntimeException) {
            Log.e("ttn.apps.handler", "getDevices err: ${e.status.code}")
            Log.d("ttn.apps.handler", "trace", e)

            when(e.status.code) {
                Status.Code.UNAUTHENTICATED -> throw UserUnauthenticatedException()
                Status.Code.PERMISSION_DENIED -> throw UserUnauthorizedException()
                else -> throw e
            }
        }
    }

    fun getDevice(appId: String, devId: String): Models.Device {
        init()
        val req = DeviceIdentifier.newBuilder().setAppId(appId).setDevId(devId).build()

        try {
            return Models.Device(blockingStub.getDevice(req))
        } catch(e: StatusRuntimeException) {
            Log.e("ttn.apps.handler", "getDevice err: ${e.status.code}")
            Log.d("ttn.apps.handler", "trace", e)

            when(e.status.code) {
                Status.Code.UNAUTHENTICATED -> throw UserUnauthenticatedException()
                Status.Code.PERMISSION_DENIED -> throw UserUnauthorizedException()
                Status.Code.NOT_FOUND -> throw NotFoundException()
                else -> throw e
            }
        }
    }

    // Only OTAA supported
    @SuppressLint("CheckResult")
    fun addDevice(appId: String,
                  appEui: ByteArray,
                  devId: String,
                  devEui: ByteArray,
                  appKey: ByteArray,
                  description: String = "",
                  latitude: Float = 0f,
                  longitude: Float = 0f,
                  altitude: Int = 0,
                  activationConstraint: Models.ActivationConstraints = Models.ActivationConstraints.OTAA
                  ) {
        init()
        val lorawanDevice = LorawanDevice.newBuilder()
                .setActivationConstraints(activationConstraint.toString().toLowerCase())
                .setDevId(devId)
                .setAppId(appId)
                .setDevEui(ByteString.copyFrom(devEui))
                .setUses32BitFCnt(true)
                .setFCntUp(0)
                .setFCntDown(0)
                .setDisableFCntCheck(false)
                .setAppEui(ByteString.copyFrom(appEui))
                .setAppKey(ByteString.copyFrom(appKey))
                .build()


        val req = Device.newBuilder()
                .setAppId(appId)
                .setDevId(devId)
                .setDescription(description)
                .setLatitude(latitude)
                .setLongitude(longitude)
                .setAltitude(altitude)
                .setLorawanDevice(lorawanDevice)
                .build()

        try {
            blockingStub.setDevice(req)
        } catch(e: StatusRuntimeException) {
            Log.e("ttn.apps.handler", "setDevice err: ${e.status.code}")
            Log.d("ttn.apps.handler", "trace", e)

            when(e.status.code) {
                Status.Code.UNAUTHENTICATED -> throw UserUnauthenticatedException()
                Status.Code.PERMISSION_DENIED -> throw UserUnauthorizedException()
                Status.Code.ALREADY_EXISTS -> throw AlreadyExistsException()
                else -> throw e
            }
        }
    }

    @SuppressLint("CheckResult")
    fun deleteDevice(appId: String, devId: String) {
        init()
        val req = DeviceIdentifier.newBuilder()
                .setAppId(appId)
                .setDevId(devId)
                .build()

        try {
            blockingStub.deleteDevice(req)
        } catch(e: StatusRuntimeException) {
            Log.e("ttn.apps.handler", "deleteDevice err: ${e.status.code}")
            Log.d("ttn.apps.handler", "trace", e)

            when(e.status.code) {
                Status.Code.UNAUTHENTICATED -> throw UserUnauthenticatedException()
                Status.Code.PERMISSION_DENIED -> throw UserUnauthorizedException()
                Status.Code.NOT_FOUND -> throw NotFoundException()
                else -> throw e
            }
        }
    }
}