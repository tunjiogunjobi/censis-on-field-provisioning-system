package com.censis.fipy.presentation.ui.activities.kotlin

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NavUtils
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.censis.fipy.R
import com.censis.fipy.network.api.Factory
import com.censis.fipy.network.api.Service
import com.censis.fipy.presentation.ui.fragments.ScanDevicesFragment
import com.censis.fipy.presentation.ui.fragments.kotlin.AuthenticationFragmentDirections
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ScanDevicesFragment.OnFragmentInteractionListener {

    companion object {
        val OAUTH2_REDIRECT_URI: Uri = Uri.parse("com.censis.fipy://oauth/callback")
    }

    lateinit var textviewToolbarSubtitle: TextView

    var apiService: Service? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        supportActionBar!!.setLogo(R.mipmap.ic_launcher)
        supportActionBar!!.setDisplayShowTitleEnabled(true)

    }


    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings |
        // File Templates.
        onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()

    }

    override fun onResume() {
        super.onResume()

        Log.d("ttn.act", "resuming")

        if(apiService == null) {
            val lastUsedService = getPreferences(Context.MODE_PRIVATE)
                    .getString("service", null)
            Log.d("ttn.act", "took service $lastUsedService")

            lastUsedService?.let{
                setApiService(it)
            }
        }
    }

    override fun onPause() {
        super.onPause()

        Log.d("ttn.act", "pausing")

        apiService?.let {
            getPreferences(Context.MODE_PRIVATE).edit()
                    .putString("service", it.serviceName).apply()
        }
    }

    @Synchronized fun setApiService(serviceName: String) {
        apiService = Factory.makeService(this, serviceName)
    }

    @Synchronized fun removeApiService() {
        apiService?.let{
            it.logout()

            getPreferences(Context.MODE_PRIVATE)
                    .edit().remove("service").apply()

            apiService = null
        }

    }

    fun resetAndGoToLogin(nav: NavController) {
        removeApiService()
        nav.navigate(AuthenticationFragmentDirections.logout())
    }
}
