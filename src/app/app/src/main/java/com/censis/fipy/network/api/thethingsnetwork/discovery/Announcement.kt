package com.censis.fipy.network.api.thethingsnetwork.discovery

import java.io.Serializable
import org.thethingsnetwork.api.discovery.Announcement as CAnnouncement

class Announcement(ann: CAnnouncement) : Serializable {
    val certificate = ann.certificate
    val netAddress = ann.netAddress
}