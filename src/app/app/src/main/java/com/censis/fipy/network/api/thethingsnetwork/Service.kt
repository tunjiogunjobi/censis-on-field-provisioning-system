package com.censis.fipy.network.api.thethingsnetwork

import android.app.Activity
import android.net.Uri
import android.util.Log
import com.censis.fipy.model.Device
import com.censis.fipy.network.api.thethingsnetwork.accounts.Models
import com.censis.fipy.network.api.thethingsnetwork.accounts.Handler as AccountsHandler
import com.censis.fipy.network.api.thethingsnetwork.applications.Handler as AppsHandler
import com.censis.fipy.network.api.thethingsnetwork.discovery.Handler as DiscoveryHandler
import com.censis.fipy.network.api.thethingsnetwork.applications.Models as AppsModels
import com.censis.fipy.model.Application
import com.censis.fipy.model.Coordinates
import com.censis.fipy.network.api.*
import com.censis.fipy.network.api.Service
import java.security.SecureRandom
import java.time.Instant

class Service(private val activity: Activity,
              handlerName: String,
              private val clientId: String,
              private val clientSecret: String)
    : Service() {

    companion object {
        const val APPS_MAX_AGE = 600L
        const val serviceName = "ttn"
    }

    private lateinit var accountsHandler: AccountsHandler
    private lateinit var token: Token
    private lateinit var appsHandler: AppsHandler
    private val discoveryHandler = DiscoveryHandler(activity)
    private val handler = discoveryHandler.getHandler(handlerName)

    private fun initToken() {
        if(!::token.isInitialized) {
            Log.d("ttn.srv", "initialising token...")
            token = Token(activity, ::refreshToken, ::exchangeToken)
            Log.d("ttn.srv", "token initialised.")
        }
    }

    private fun initAccounts() {
        initToken()
        if(!::accountsHandler.isInitialized) {
            Log.d("ttn.srv", "initialising accounts...")
            accountsHandler = AccountsHandler(
                    clientId,
                    clientSecret,
                    { token.bearerToken },
                    { token.appBearerToken }
            )
            Log.d("ttn.srv", "accounts initialised.")
        }
    }

    private fun initApps() {
        initAccounts()
        if(!::appsHandler.isInitialized) {
            Log.d("ttn.srv", "initialising apps...")
            appsHandler = AppsHandler(
                    handler.netAddress,
                    handler.certificate
            ) { token.appBearerToken }
            Log.d("ttn.srv", "apps initialised.")
        }
    }

    override val isAuthenticated: Boolean get() {
        initToken()
        return token.isAuthenticated
    }

    private val appsCache = Cache<List<Application>>(APPS_MAX_AGE)

    override val serviceName = "ttn"

    override fun buildAuthenticationUri(redirectUri: Uri): Uri {
        return Uri.Builder()
                .scheme("https")
                .authority("account.thethingsnetwork.org")
                .appendPath("users")
                .appendPath("authorize")
                .appendQueryParameter("client_id", clientId)
                .appendQueryParameter("response_type", "code")
                .appendQueryParameter("redirect_uri", redirectUri.toString())
                .build()
    }

    @Synchronized override fun authenticate(code: String): Boolean {
        initAccounts()
        Log.d("ttn", "exchanging authcode for bearer token")
        val req = accountsHandler.getBearerToken(code).execute()
        if (req.isSuccessful) {
            token.set(Models.Token(req.body()))
            Log.d("ttn", "new bearer token: ${token.bearerToken}")
            return true
        }

        return false
    }

    @Synchronized override fun logout(): Boolean {
        initToken()
        try {
            token.invalidate()
            discoveryHandler.shutdown()
            if(::appsHandler.isInitialized)
                appsHandler.shutdown()
            return true
        } catch(e: Exception) {}

        return false
    }

    @Synchronized private fun refreshToken(token: Models.Token): Models.Token? {
        initAccounts()
        Log.d("ttn", "refreshing bearer token ${token.refreshToken} ${accountsHandler}")
        val req = accountsHandler.refreshBearerToken(token.refreshToken).execute()
        if (req.isSuccessful)
            return Models.Token(req.body())

        Log.e("ttn.token", "failed to refresh token: ${req.errorBody().string()}")
        return null
    }

    @Synchronized private fun exchangeToken(token: Models.Token): Models.Token? {
        initAccounts()
        Log.d("ttn", "exchanging accounts bearer token for apps bearer token")
        val apps = if(appsCache.isExpired != false) getApplications() else appsCache.get()

        if(apps != null) {
            val req = accountsHandler.exchangeToken(
                    token.accessToken,
                    apps.map { "apps:${it.id}" } + "apps" + "profile"
            ).execute()

            if (req.isSuccessful)
                return Models.Token(req.body())

            Log.e("ttn.appToken", "could not exchange the token: ${req.errorBody().string()}")
        }

        return null
    }

    @Throws(UserUnauthenticatedException::class,
            UserUnauthorizedException::class)
    override fun getApplications(): List<Application>? {
        initAccounts()
        val req = accountsHandler.getApplications().execute()
        when {
            req.isSuccessful -> {
                val body = req.body().map { Models.Application(it) }
                appsCache.set(body)
                return body
            }
            req.code() == 401 -> {
                token.invalidate()
                throw UserUnauthenticatedException()
            }
            req.code() == 403 -> throw UserUnauthorizedException()
            else -> {
                Log.d("ttn.service", req.errorBody().string())
            }
        }

        return null
    }

    @Throws(UserUnauthenticatedException::class,
            UserUnauthorizedException::class,
            NotFoundException::class)
    override fun getApplication(appId: String): Application? {
        initAccounts()
        val req = accountsHandler.getApplication(appId).execute()

        when {
            req.isSuccessful -> return Models.Application(req.body())
            req.code() == 401 -> {
                token.invalidate()
                throw UserUnauthenticatedException()
            }
            req.code() == 403 -> throw UserUnauthorizedException()
            req.code() == 404 -> throw NotFoundException()
            else -> {
                Log.d("ttn.service", req.errorBody().string())
            }
        }

        return null
    }

    @Throws(UserUnauthenticatedException::class,
            UserUnauthorizedException::class)
    override fun getDevices(app: Application): List<Device>? {
        initApps()
        try {
            return appsHandler.getDevices(app.id)
        } catch (e: UserUnauthenticatedException) {
            token.invalidate()
            throw UserUnauthenticatedException()
        }
    }

    @Throws(UserUnauthenticatedException::class,
            UserUnauthorizedException::class,
            NotFoundException::class)
    override fun getDevice(app: Application, id: String): Device? {
        initApps()
        try {
            return appsHandler.getDevice(app.id, id)
        } catch (e: UserUnauthenticatedException) {
            token.invalidate()
            throw UserUnauthenticatedException()
        }
    }

    // FIXME: Caution! If an already existing device id is supplied it will edit the device!!
    @Throws(UserUnauthenticatedException::class,
            UserUnauthorizedException::class,
            AlreadyExistsException::class,
            UnsupportedActivationConstraint::class)
    override fun addDevice(app: Application,
                           id: String,
                           devEui: ByteArray,
                           appEui: ByteArray?,
                           description: String,
                           coordinates: Coordinates,
                           altitude: Int,
                           activationConstraint: String,
                           devAddress: ByteArray?,
                           networkSessionKey: ByteArray?,
                           appSessionKey: ByteArray?,
                           appKey: ByteArray?
    ): ByteArray {
        initApps()

        val actConst = when (activationConstraint.toLowerCase()) {
            "otaa" -> AppsModels.ActivationConstraints.OTAA
            //"abp" -> AppsModels.ActivationConstraints.ABP
            else -> throw UnsupportedActivationConstraint()
        }

        val appEuiFinal: ByteArray = when {
            appEui != null -> appEui
            app.euis.isNotEmpty() -> app.euis[0]
            else -> throw IllegalArgumentException("no App EUI set, or applications with at least one App EUI.")
        }

        val appKeyFinal: ByteArray = when {
            appKey != null -> appKey
            else -> generateRandomBytes(16)
        }

        if(appEuiFinal.size != 8) throw IllegalArgumentException("the app EUI should be 8 bytes long")
        if(devEui.size != 8) throw IllegalArgumentException("the dev EUI should be 8 bytes long")
        if(appKeyFinal.size != 16) throw IllegalArgumentException("the app key should be 16 bytes long")

        try {
            appsHandler.addDevice(
                    app.id, appEuiFinal,
                    id, devEui,
                    appKeyFinal,
                    description,
                    coordinates.latitude,
                    coordinates.longitude,
                    altitude,
                    actConst)
        } catch (e: UserUnauthenticatedException) {
            token.invalidate()
            throw UserUnauthenticatedException()
        }

        return appKeyFinal
    }

    @Throws(UserUnauthenticatedException::class,
            UserUnauthorizedException::class,
            NotFoundException::class)
    override fun deleteDevice(app: Application, id: String) {
        initApps()

        try {
            appsHandler.deleteDevice(app.id, id)
        } catch (e: UserUnauthenticatedException) {
            token.invalidate()
            throw UserUnauthenticatedException()
        }
    }

    override fun getUsername(): String {
        return token.username
    }

    override fun getEmailAddress(): String {
        return token.emailAddress
    }

    // random secure bytes generation
    private fun generateRandomBytes(size: Int): ByteArray {
        val bytes = ByteArray(size)
        SecureRandom.getInstanceStrong().nextBytes(bytes)
        return bytes
    }

    // internal class for caching apps to exchange tokens
    class Cache<T>(private val max_age: Long) {
        private var setOn: Long? = null
        private var cached: T? = null

        val isExpired: Boolean? get() = setOn?.let{
            Instant.now().epochSecond > it + max_age
        }

        fun set(obj: T) {
            cached = obj
            setOn = Instant.now().epochSecond
        }

        fun get(): T? {
            return cached
        }
    }
}
