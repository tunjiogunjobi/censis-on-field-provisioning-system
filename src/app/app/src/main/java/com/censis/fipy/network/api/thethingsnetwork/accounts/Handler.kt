package com.censis.fipy.network.api.thethingsnetwork.accounts

import okhttp3.Credentials
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Handler(
        private val clientId: String,
        private val clientSecret: String,
        private val getBearerToken: () -> String,
        private val getAppBearerToken: () -> String
) {
    companion object {
        const val ACCOUNT_SERVER = "https://account.thethingsnetwork.org"
        const val API_BASE_URL = "https://account.thethingsnetwork.org/api/v2/"
        const val REDIRECT_URI = "com.censis.fipy://oauth/callback"
    }

    private val accountsService = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(ACCOUNT_SERVER)
            .build().create(Service::class.java)

    private val apiService = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(API_BASE_URL)
            .build().create(Service::class.java)

    fun getBearerToken(authCode: String): Call<Models.TokenData> {
        val body = Models.TokenRequest(
                code = authCode,
                grant_type = "authorization_code",
                redirect_uri = REDIRECT_URI
        )

        return accountsService.getBearerToken(Credentials.basic(clientId, clientSecret), body)
    }

    fun exchangeToken(bearerToken: String, scope: List<String>): Call<Models.TokenData> {
        return accountsService.exchangeToken("Bearer $bearerToken", Models.ExchangeTokenRequest(scope))
    }

    fun refreshBearerToken(refreshToken: String): Call<Models.TokenData> {
        val body = Models.TokenRefreshRequest(
                refresh_token = refreshToken,
                grant_type = "refresh_token",
                redirect_uri = REDIRECT_URI
        )

        return accountsService.refreshBearerToken(Credentials.basic(clientId, clientSecret), body)
    }

    fun getApplications(): Call<List<Models.ApplicationData>> {
        return apiService.getApplications("Bearer ${getBearerToken()}")
    }

    fun getApplication(appId: String): Call<Models.ApplicationData> {
        return apiService.getApplication("Bearer ${getAppBearerToken()}", appId)
    }
}