package com.censis.fipy.network.api.thethingsnetwork.accounts

import android.util.Base64
import com.censis.fipy.Utils
import com.google.gson.Gson
import com.censis.fipy.model.Application as IApplication

object Models {
    internal data class TokenRequest (
            val grant_type: String,
            val redirect_uri: String,
            val code: String
    )

    internal data class TokenRefreshRequest (
            val grant_type: String,
            val redirect_uri: String,
            val refresh_token: String
    )

    internal data class ExchangeTokenRequest (
            val scope: List<String>
    )

    data class TokenData (
            val refresh_token: String,
            val access_token: String
    )

    class Token (data: TokenData) {
        val accessToken = data.access_token
        val refreshToken = data.refresh_token

        private val payload: TokenPayload

        init {
            val components = accessToken.split(".")
            val rawPayload = Base64.decode(components[1], Base64.URL_SAFE)
            payload = Gson().fromJson(
                    String(rawPayload, Charsets.UTF_8),
                    TokenPayload::class.java
            )
        }

        val exp = payload.exp
        val username = payload.username
        val email = payload.email
    }

    data class TokenPayload (
            val exp: Long,
            val username: String,
            val email: String
    )

    data class ApplicationData(
            val id: String,
            val name: String,
            val euis: List<String>
    )

    class Application(data: ApplicationData) : IApplication() {

        override val id: String = data.id

        override val name: String = data.name

        override val euis: List<ByteArray> = data.euis.map { Utils.hexToBytes(it) }
    }
}