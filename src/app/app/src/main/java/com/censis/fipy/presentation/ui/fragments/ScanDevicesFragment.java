package com.censis.fipy.presentation.ui.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.censis.fipy.R;
import com.censis.fipy.model.Application;
import com.censis.fipy.network.ble.BleScanner;
import com.censis.fipy.network.ble.BluetoothLeScan;
import com.censis.fipy.presentation.adapter.ScanListAdapter;
import com.censis.fipy.presentation.ui.activities.kotlin.MainActivity;
import com.censis.fipy.presentation.viewmodels.MainViewModel;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class ScanDevicesFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String SCANNING = "scanning";

    private Application app;

    private OnFragmentInteractionListener mListener;

    //
    private BluetoothManager btManager;
    private BluetoothAdapter btAdapter;

    private BleScanner mBleScanner;

    private final static int REQUEST_ENABLE_BT = 1;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    private MainViewModel viewModel;

    private ScanListAdapter adapter;

    private boolean mScanning;


    public ScanDevicesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     *
     * @return A new instance of fragment ScanDevicesFragment.
     */
    public static ScanDevicesFragment newInstance(String param1, String param2) {
        ScanDevicesFragment fragment = new ScanDevicesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (Application) getArguments().getSerializable("app");

        adapter = new ScanListAdapter();
        adapter.setApplication(app);

        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        viewModel.getAllFipyDevices().observe(this, devices -> {
            // Update the cached copy of the devices in the adapter.
            adapter.setDevices(devices);
        });

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onPause() {
        super.onPause();
        if(mScanning)
            mBleScanner.stopScanning();
    }



    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mScanning = (savedInstanceState == null)? false: (Boolean) savedInstanceState.get(SCANNING);

        return inflater.inflate(R.layout.fragment_scan_device, container, false);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(SCANNING,mScanning);

        //save title state
        outState.putString("title",getActivity().getTitle().toString());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        RecyclerView recyclerView = view.findViewById(R.id.include_content_main).findViewById(R.id.recyclerview);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);

        mScanning = (savedInstanceState == null)? false: (Boolean) savedInstanceState.get(SCANNING);
        if(savedInstanceState != null){

        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btManager = (BluetoothManager)getContext().getSystemService(Context.BLUETOOTH_SERVICE);
        btAdapter = btManager.getAdapter();
        mBleScanner = new BluetoothLeScan(getActivity());

        app = (Application) getArguments().getSerializable("app");




        MainActivity mainActivity = (MainActivity) getActivity();
        if ((savedInstanceState == null)) {
            mainActivity.setTitle("Scan For Devices");
        } else {
            mainActivity.setTitle("Available Devices");
        }
        mainActivity.getSupportActionBar().setDisplayUseLogoEnabled(false);
        mainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if (btAdapter != null && !btAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent,REQUEST_ENABLE_BT);
        }

        // Make sure we have access coarse location enabled, if not, prompt the user to enable it
        if (getActivity().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("This app needs location access");
            builder.setMessage("Please grant location access so this app can detect peripherals.");
            builder.setPositiveButton(android.R.string.ok, null);
            builder.setOnDismissListener(dialog -> requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION));
            builder.show();
        }
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (menu.size() == 0)
            inflater.inflate(R.menu.menu_main, menu);


    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);

        if (!mScanning) {
            menu.findItem(R.id.action_menu_stop).setVisible(false);
            menu.findItem(R.id.action_menu_refresh).setActionView(null);
        } else {
            MenuItem stopMenu = menu.findItem(R.id.action_menu_stop);
            stopMenu.setVisible(true);
            stopMenu.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            menu.findItem(R.id.action_menu_refresh).setActionView(R.layout.actionbar_indeterminate_progress);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                NavHostFragment.findNavController(this).navigateUp();
                return true;
            case R.id.action_menu_refresh:
                //viewModel.clear();
                mBleScanner.clear();
                mScanning = true;
                //viewModel.startScanning();
                mBleScanner.startScanning();
                Toast toast = Toast.makeText(getActivity(),"Scanning for ble devices",Toast.LENGTH_SHORT);
                toast.show();
                getActivity().invalidateOptionsMenu();
                return true;
            case R.id.action_menu_stop:
                mScanning = false;
                //viewModel.stopScanning();
                mBleScanner.stopScanning();
                getActivity().invalidateOptionsMenu();
                return true;
            case R.id.action_menu_settings:
                return true;
            case R.id.menu_applicatioions:
                NavHostFragment.findNavController(this).navigate(R.id.ttnApplicationsFragment);
                        return true;
            default:
                return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    System.out.println("coarse location permission granted");
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Functionality limited");
                    builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons when in the background.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(dialog -> {
                    });
                    builder.show();
                }
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and dev_id
        void onFragmentInteraction(Uri uri);
    }



    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            final int position = viewHolder.getAdapterPosition();
            if (direction == ItemTouchHelper.LEFT){
                adapter.addDevice(position);
            }
        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            try {

                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE){
                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;
                    Paint paint = new Paint();
                    paint.setColor(Color.parseColor("#D32F2F"));

                    RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(),(float) itemView.getRight(), (float) itemView.getBottom());
                    c.drawRect(background,paint);
                    icon = BitmapFactory.decodeResource(getResources(), R.drawable.censis_logo);
                    RectF destination = new RectF((float) itemView.getRight() - 2*width ,(float) itemView.getTop() + width,(float) itemView.getRight() - width,(float)itemView.getBottom() - width);

                    c.drawBitmap(icon,null,destination ,paint);
                }else {
                    super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };


    public Application getApp(){
        return app;
    }

}
