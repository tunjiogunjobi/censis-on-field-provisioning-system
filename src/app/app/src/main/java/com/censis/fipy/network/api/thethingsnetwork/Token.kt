package com.censis.fipy.network.api.thethingsnetwork

import android.app.Activity
import android.content.Context
import android.os.AsyncTask
import android.util.Log
import com.censis.fipy.network.api.thethingsnetwork.accounts.Models
import com.google.gson.Gson
import java.time.Instant
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

class Token(activity: Activity,
            private val refreshTokenHandler: (Models.Token) -> Models.Token?,
            private val exchangeTokenHandler: (Models.Token) -> Models.Token?) {

    companion object {
        const val TOKEN_PREF = "userToken"
    }

    @Volatile private var userToken: Models.Token? = null
        set(value) {
            field = value

            if (value != null)
                scheduleRefresh()
            else
                cancelScheduledRefresh()
        }

    @Volatile private var appToken: Models.Token? = null

    private val executor = Executors.newSingleThreadScheduledExecutor()
    private var refresher: ScheduledFuture<*>? = null

    private val prefs = activity.getPreferences(Context.MODE_PRIVATE)

    val isAuthenticated get() = userToken?.let {
        it.exp - Instant.now().epochSecond > 0
    } ?: false

    val bearerToken: String get() {
        tokenRefresher?.let { it.get() }

        return userToken?.let {
            it.accessToken
        } ?: ""
    }

    val username get() = userToken?.let{ it.username } ?: ""
    val emailAddress get() = userToken?.let{ it.email } ?: ""

    val appBearerToken: String get() {
        if(isAuthenticated) {
            return userToken?.let {
                Log.d("ttn.token", "getting bearer token: $appToken")
                if(appToken == null || Instant.now().epochSecond - (appToken?.exp ?: 0) > 0) {
                    appToken = exchangeTokenHandler(it)
                    return appToken?.accessToken ?: ""
                }

                return appToken?.accessToken ?: ""
            } ?: ""
        }

        return ""
    }

    init {
        userToken = loadToken()
    }

    @Synchronized fun set(token: Models.Token) {
        userToken = token
        saveToken()
    }

    @Synchronized fun invalidate() {
        userToken = null
        removeToken()
    }

    object Refresher : AsyncTask<() -> Unit, Unit, Unit>() {
        override fun doInBackground(vararg cbs: () -> Unit) {
            for(cb in cbs) {
                cb()
            }
        }
    }

    private var tokenRefresher: Refresher? = null

    private fun refreshToken() {
        userToken?.let{
            tokenRefresher = Refresher.execute({
                val newToken = refreshTokenHandler(it)
                if(newToken != null)
                    set(newToken)
                else
                    invalidate()
            }) as Refresher
        }
    }

    /**
     * Activity preferences operations
     */

    @Synchronized private fun loadToken(): Models.Token? {
        val rawToken = prefs.getString(TOKEN_PREF, "")
        Log.d("ttn.token", "loading user token from storage: $rawToken")
        return Gson().fromJson(rawToken, Models.Token::class.java)
    }

    @Synchronized private fun saveToken() {
        userToken?.let {
            prefs.edit().putString(
                    TOKEN_PREF,
                    Gson().toJson(it, Models.Token::class.java)
            ).apply()
            Log.d("ttn.token", "saving token to storage")
        }
    }

    @Synchronized private fun removeToken() {
        if(prefs.contains(TOKEN_PREF))
            prefs.edit().remove(TOKEN_PREF).apply()
        Log.d("ttn.token", "removing token from storage")
    }

    /**
     * Refresh scheduler operations
     */

    @Synchronized private fun scheduleRefresh() {
        userToken?.let{ jwt ->
            val expiresIn = jwt.exp - Instant.now().epochSecond
            if(expiresIn > 0) {
                refresher?.let {
                    if(!it.isDone && !it.isCancelled) it.cancel(true)
                }

                refresher = executor.schedule(::refreshToken, expiresIn, TimeUnit.SECONDS)
            } else
                refreshToken()
        }
    }

    @Synchronized private fun cancelScheduledRefresh() {
        refresher?.let {
            if (!it.isDone && !it.isCancelled) it.cancel(true)
        }
    }
}