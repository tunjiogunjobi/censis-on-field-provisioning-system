package com.censis.fipy.network.api

class UnsupportedActivationConstraint : Exception {
    constructor(message: String) : super(message)
    constructor(): super()
}