package com.censis.fipy.network.ble;


/**
 * The BleScanner interace provides the follwing methods:
 *  - startScanning, stopScanning, clear.
 */
public interface BleScanner {

    /**
     * starts scanning for ble devices
     */
    void startScanning();

    /**
     *  Stop scanning for ble devices
     */
    void stopScanning();

    /**
     * Clear devices discovered during scanning from list.
     */
    void clear();


}
