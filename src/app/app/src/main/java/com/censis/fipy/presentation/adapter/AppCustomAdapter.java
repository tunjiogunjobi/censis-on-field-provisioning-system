package com.censis.fipy.presentation.adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.censis.fipy.R;
import com.censis.fipy.Utils;
import com.censis.fipy.model.Application;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

public class AppCustomAdapter extends RecyclerView.Adapter<AppCustomAdapter.CustomViewHolder> {
    private List<Application> mApplications;

    public static class CustomViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextViewAppName, mTextviewAppId;

        public CustomViewHolder(final View view){
            super(view);
            mTextViewAppName = view.findViewById(R.id.textView_app_name);
            mTextviewAppId = view.findViewById(R.id.textView_app_id);
        }
    }


    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.recyclerview_item_applications,parent,false);

        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        if (mApplications != null && !mApplications.isEmpty()) {
            final Application application = mApplications.get(position);

            String name = application.getName();
            String id = application.getId();

            if(name.isEmpty()) name = "Unknown";

            holder.mTextViewAppName.setText(name);
            holder.mTextviewAppId.setText(id);

            final View view = holder.mTextViewAppName.getRootView();

            view.setOnClickListener(v -> {
                Bundle bundle = new Bundle();
                bundle.putSerializable("app", application);
                Navigation.findNavController(view).navigate(R.id.action_ttnApplicationsFragment_to_appDetailsFragment, bundle);
            });
        } else {
            // Covers the case of data not being ready yet.
            holder.mTextViewAppName.setText(R.string.no_apps);
        }
    }

    @Override
    public int getItemCount() {
        if (mApplications != null)
            return mApplications.size();
        else return 0;

    }

    public void setApplications(List<Application> applications){
        mApplications = applications;
        notifyDataSetChanged();
    }

    void clear(){
        if(mApplications != null)
            mApplications.clear();
    }
}
