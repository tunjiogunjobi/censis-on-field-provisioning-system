package com.censis.fipy.network.api

class UserUnauthorizedException : Exception {
    constructor(message: String) : super(message)
    constructor(): super()
}