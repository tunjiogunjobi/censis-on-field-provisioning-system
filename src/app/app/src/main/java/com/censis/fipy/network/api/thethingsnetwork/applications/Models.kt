package com.censis.fipy.network.api.thethingsnetwork.applications

import com.censis.fipy.model.Coordinates
import java.time.Instant
import com.censis.fipy.model.Device as IDevice
import org.thethingsnetwork.api.handler.Device as TtnDevice

object Models {
    class Device(dev: TtnDevice) : IDevice() {
        override val lastSeen: Instant? = if (dev.lorawanDevice.lastSeen > 0)
            Instant.ofEpochMilli(dev.lorawanDevice.lastSeen/1000000L)
        else
            null

        override val appEui: ByteArray = dev.lorawanDevice.appEui.toByteArray()
        override val devEui: ByteArray = dev.lorawanDevice.devEui.toByteArray()
        override val appKey: ByteArray = dev.lorawanDevice.appKey.toByteArray()

        override val devAddress: ByteArray = dev.lorawanDevice.devAddr.toByteArray()

        override val appSessionKey: ByteArray = dev.lorawanDevice.appSKey.toByteArray()
        override val networkSessionKey: ByteArray = dev.lorawanDevice.nwkSKey.toByteArray()

        override val appId: String = dev.appId
        override val devId: String = dev.devId

        override val description: String = dev.description

        override val altitude: Int = dev.altitude
        override val position: Coordinates? = if(!dev.latitude.equals(.0f) && !dev.longitude.equals(.0f))
            Coordinates(dev.latitude, dev.longitude)
        else
            null
    }

    enum class ActivationConstraints {
        OTAA, ABP, WORLD, LOCAL, PRIVATE, TESTING
    }
}