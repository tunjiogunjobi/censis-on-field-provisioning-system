package com.censis.fipy.presentation.ui.fragments.kotlin

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.censis.fipy.R
import android.os.AsyncTask
import android.util.Log
import android.view.*
import android.widget.TextView
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.censis.fipy.model.Application
import com.censis.fipy.network.api.UserUnauthenticatedException
import com.censis.fipy.network.api.thethingsnetwork.Service as ApiService
import com.censis.fipy.presentation.adapter.AppCustomAdapter
import com.censis.fipy.presentation.ui.activities.kotlin.MainActivity
import com.google.gson.Gson
import org.jetbrains.anko.find
import org.jetbrains.anko.toast
import java.io.Serializable
import java.time.Instant
import java.io.ObjectOutputStream
import java.lang.reflect.InvocationTargetException


class ApplicationsFragment: Fragment() {

    companion object {
        const val MAX_AGE = 300L // 5 minutes
    }

    var mApplications: List<Application> = ArrayList()
    var mAppsAge: Instant? = null

    private lateinit var mListAdapter: AppCustomAdapter
    lateinit var recyclerView: RecyclerView
    lateinit var container: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        mListAdapter = AppCustomAdapter()
    }
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        return inflater.inflate(com.censis.fipy.R.layout.fragment_ttn_app,
                container,
                false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        container = view.findViewById(R.id.pullToRefreshApps)
        recyclerView = container.findViewById(R.id.recyclerview_applications)
        recyclerView.adapter = mListAdapter
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.visibility = View.GONE

        container.setOnRefreshListener {
            loadApps()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val mainActivity = activity as MainActivity
        mainActivity.setTitle(R.string.apps)
        mainActivity.supportActionBar!!.setDisplayUseLogoEnabled(false)
        mainActivity.supportActionBar!!.setDisplayHomeAsUpEnabled(false)
    }

    private fun loadApps() {
        val mainActivity = activity as MainActivity

        view?.findViewById<TextView>(R.id.appsError)?.visibility = View.GONE
        container.isRefreshing = true
        AsyncTask.execute {
            try {
                mainActivity.apiService?.getApplications()?.let {
                    mApplications = it
                    mAppsAge = Instant.now()
                }

                activity?.runOnUiThread {
                    mListAdapter.setApplications(mApplications)

                    Log.d("ttn", "mApplication size is now: " + mApplications.size )

                    if(mApplications.isEmpty()) {
                        recyclerView.visibility = View.GONE
                        view?.findViewById<TextView>(R.id.noAppsView)?.visibility = View.VISIBLE
                    } else {
                        recyclerView.visibility = View.VISIBLE
                        view?.findViewById<TextView>(R.id.noAppsView)?.visibility = View.GONE
                    }
                }
            } catch(e: UserUnauthenticatedException) {
                activity?.runOnUiThread {
                    context?.toast(R.string.user_unauthenticated)
                }
                mainActivity.resetAndGoToLogin(NavHostFragment.findNavController(this))
                TODO("recover on timeout or no network")
            } catch(e: Exception) {
                activity?.runOnUiThread {
                    view?.findViewById<TextView>(R.id.appsError)?.visibility = View.VISIBLE
                    view?.findViewById<TextView>(R.id.noAppsView)?.visibility = View.GONE
                    recyclerView.visibility = View.GONE
                }
            }

            activity?.runOnUiThread {
                container.isRefreshing = false
            }
        }
    }

    override fun onResume() {
        super.onResume()

        Log.d("ttn.apps", "resuming")

        if(mAppsAge?.let { Instant.now().isAfter(it.plusSeconds(MAX_AGE)) } != true)
            loadApps()
        else {
            container.isRefreshing = true
            mListAdapter.setApplications(mApplications)
            container.isRefreshing = false
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        if (menu.size() == 0)
            inflater.inflate(R.menu.menu_apps, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.details_logout -> (activity!! as MainActivity).resetAndGoToLogin(
                    NavHostFragment.findNavController(this)
            )
        }

        return super.onOptionsItemSelected(item)
    }
}