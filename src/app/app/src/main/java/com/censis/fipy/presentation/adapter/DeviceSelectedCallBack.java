package com.censis.fipy.presentation.adapter;

import android.os.Bundle;

import com.censis.fipy.model.Application;
import com.censis.fipy.model.FipyDevice;

public interface DeviceSelectedCallBack {

    void setApplication(Application app);
}
