package com.censis.fipy.presentation.ui.fragments.kotlin

import android.bluetooth.*
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.censis.fipy.CensisFipyUUID
import com.censis.fipy.R
import com.censis.fipy.Utils
import com.censis.fipy.model.Application
import com.censis.fipy.model.Coordinates
import com.censis.fipy.model.FipyDevice
import com.censis.fipy.network.api.AlreadyExistsException
import com.censis.fipy.network.api.NotFoundException
import com.censis.fipy.presentation.ui.activities.kotlin.MainActivity
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import io.grpc.StatusRuntimeException
import kotlinx.android.synthetic.main.fragment_device_details.*
import java.nio.ByteBuffer
import java.util.*

class DeviceRegistrationFragments:Fragment(),View.OnClickListener {

    private var mGoogleApiClient: GoogleApiClient? = null
    private var mFusedLocationProviderClient: FusedLocationProviderClient? = null

    private val APP:String = "app"
    private var mFipyDevice:FipyDevice?=null

    private var inputDevId: TextInputEditText? = null
    private var inputDevEUI: TextInputEditText? = null
    private var inputAppKey: TextInputEditText? = null
    private var textView_app_eui: AppCompatTextView? = null
    private var textInputLayoutDevId: TextInputLayout? = null
    private var textInputLayoutDevEui: TextInputLayout? = null

    private var textInputTimeInterval: TextInputEditText? = null


    private var buttonRegister: AppCompatButton? = null

    private val FIPY_DEVICE = "fipyDevice"

    private lateinit var application:Application
    private lateinit var mDevEUI: ByteArray

    private var mBleMacAddress: String? = null


    private lateinit var mBluetoothAdapter: BluetoothAdapter
    private lateinit var mBluetoothGatt: BluetoothGatt

    private var mConnectionState: Int? = null

    private var mConnected: Boolean? = null

    private var mAppKey: ByteArray? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true);


        mBluetoothAdapter = (activity!!.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter


        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity!!);




        val mainActivity = activity as MainActivity
        mainActivity.setTitle(R.string.app_name)
        mainActivity.supportActionBar!!.setDisplayUseLogoEnabled(false)
        mainActivity.supportActionBar!!.setDisplayHomeAsUpEnabled(true)


        val toolbar:androidx.appcompat.widget.Toolbar? = activity!!.findViewById(R.id.toolbar) as Toolbar

        (toolbar?.findViewById(R.id.textview_toolbar_subtitle) as TextView).text = "REGISTER DEVICE"


        //pre-populate registration form
        arguments.let {

            mBleMacAddress = it?.getString("bleMacAddress")

            Log.d("ttn", "ble address is:" + mBleMacAddress!!.replace(":",""))


            application = it?.getSerializable("app") as Application

            mFipyDevice = it.getSerializable(FIPY_DEVICE) as FipyDevice
            inputDevId!!.text =  SpannableStringBuilder(mFipyDevice!!.dev_id)

            textView_app_eui!!.text = Utils.bytesToHex(application.euis.get(0))


            Log.d("ttn", "app in device reg is: " + application.id  )

        }

        AsyncTask.execute {
            mConnected = connect(mFipyDevice?.bleMacAddress!!)

        }
        var mConnected = connect(mFipyDevice?.bleMacAddress!!)

        Log.d("ttn", "mconnected id :" + mConnected)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_device_registration,container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        inputDevId = view.findViewById(R.id.text_input_dev_id) as TextInputEditText
        inputDevEUI = view.findViewById(R.id.text_input_dev_eui)
        inputAppKey = view.findViewById(R.id.text_input_app_key)

        textInputTimeInterval = view.findViewById(R.id.text_input_time_interval)

        textInputLayoutDevId = view.findViewById(R.id.text_input_layout_dev_id)
        textInputLayoutDevEui = view.findViewById(R.id.text_input_layout_dev_eui)


        textView_app_eui = view.findViewById(R.id.textView_app_eui) as AppCompatTextView


        buttonRegister = view.findViewById(R.id.button_register)
        buttonRegister!!.setOnClickListener(this)

    }

    override fun onDestroy() {
        super.onDestroy()
        if (mBluetoothGatt != null){
            mBluetoothGatt.disconnect()
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                NavHostFragment.findNavController(this).navigateUp()
                return true
            }
        }

        return false
    }


    val mGattCallback = (object : BluetoothGattCallback(){

        override fun onCharacteristicRead(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
            super.onCharacteristicRead(gatt, characteristic, status)
            when(status){
                BluetoothGatt.GATT_SUCCESS -> {
                    mDevEUI = characteristic!!.value
                    activity!!.runOnUiThread {
                        inputDevEUI!!.text = SpannableStringBuilder(Utils.bytesToHex(mDevEUI))
                        //textInputLayoutDevEui!!.hint=""
                        inputDevEUI!!.isEnabled = false

                    }

                    Log.d("ttn", "On read: " + characteristic!!.value)

                }
            }


        }

        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            when(newState){
                BluetoothAdapter.STATE_CONNECTED-> {
                    mConnectionState = BluetoothAdapter.STATE_CONNECTED
                    gatt?.discoverServices()
                    Log.d("ttn", "onconnection state changed, connected")

                }
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            super.onServicesDiscovered(gatt, status)
            Log.d("ttn", gatt.services.size.toString())
            val devEuiCharacteristics = gatt
                    .getService(UUID.fromString(CensisFipyUUID.PROVISIONING_SERVICE_UUID)).
                    getCharacteristic(UUID.fromString(CensisFipyUUID.DEV_EUI_UUID))
            Log.d("ttn", "dev eui xter is: " + devEuiCharacteristics.uuid)
            gatt.readCharacteristic(devEuiCharacteristics)

        }

        override fun onCharacteristicWrite(gatt: BluetoothGatt,
                                           characteristic: BluetoothGattCharacteristic?,
                                           status: Int) {
            super.onCharacteristicWrite(gatt, characteristic, status)

            when(status){
                BluetoothGatt.GATT_SUCCESS ->{

                }
            }


        }

    })


    private fun connect(address : String):Boolean {

        val device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.d("ttn", "Device not found.  Unable to connect.")
            return false
        }
        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        mBluetoothGatt = device.connectGatt(activity, false, mGattCallback);
        if(mBluetoothGatt.connect()){
        // refreshDeviceCache(mBluetoothGatt);
            Log.d("ttn", "Trying to create a new connection.");
            mConnectionState = BluetoothAdapter.STATE_CONNECTING
            Log.d("ttn", "connection state" + mConnectionState.toString())
            return true
        }
        return false
    }

    private fun writeCharateristics(appKey: ByteArray, appEUI: ByteArray, interval: Int){

        val gattService = mBluetoothGatt.getService(UUID.fromString(CensisFipyUUID.PROVISIONING_SERVICE_UUID))
        val appKeyCharacteristic = gattService.getCharacteristic(UUID.fromString(CensisFipyUUID.APP_KEY_UUID))

        val appEUICharacteristic = gattService.getCharacteristic(UUID.fromString(CensisFipyUUID.APP_EUI_UUID))

        val appIntervalCharacteristics = gattService.getCharacteristic(UUID.fromString(CensisFipyUUID.TIME_INTERVAL_UUID))

        appKeyCharacteristic.value = appKey
        appEUICharacteristic.value = appEUI
        val timeIntervalBuffer = ByteBuffer.allocate(4)
        timeIntervalBuffer.putInt(interval)
        appIntervalCharacteristics.value = timeIntervalBuffer.array()

        Log.d("devadd", "timeintbytesarr ${timeIntervalBuffer.array().toString()}")


        val status = mBluetoothGatt.writeCharacteristic(appKeyCharacteristic)
        val status2 = mBluetoothGatt.writeCharacteristic(appEUICharacteristic)
        val status3 = mBluetoothGatt.writeCharacteristic(appIntervalCharacteristics)

        Log.d("ttn", "write status is: " + status)
        if(status && status2){
            activity?.runOnUiThread{
                Toast.makeText(activity,"AppKey and AppEUI saved",Toast.LENGTH_LONG).show()
            }
        }


    }


    override fun onClick(v: View?) {

            val apiService = (activity as MainActivity).apiService

            val id = inputDevId!!.text.toString()

            AsyncTask.execute{

                //check if id exist already
                try {
                    apiService!!.getDevice(application,id)

                }catch (e: NotFoundException){
                    //device id does not exist
                    Log.d("ttn", "device id does not exist")

                    try {

//                      addDevice(application, id, mDevEUI,
//                          description = "bleMacAddress:${Utils.bytesToHex(bleMac)}",
//                          coordinates = Coordinates(lat, lon))
                        var longitude = 0.0f
                        var latitude = 0.0f

                        mFusedLocationProviderClient!!.lastLocation
                                .addOnSuccessListener{
                                    longitude =  it?.longitude!!.toFloat()
                                    latitude = it?.latitude!!.toFloat()

                                    Log.d("ttn", "last location lat is: " + latitude)
                                }


                        mAppKey = apiService!!.addDevice(application, id = id,devEui =  mDevEUI,
                                description = "bleMacAddress:${mBleMacAddress!!.replace(":","")}"
                                ,coordinates = Coordinates(latitude,longitude))
                        //mFipyDevice!!.lorawanDevice?.devEui = mDevEUI
                        if (mAppKey != null){

                            Log.d("ttn","App key is" + mAppKey.toString())

                            val timeInterval: Int
                            if(textInputTimeInterval?.text.toString().isNotEmpty()) {
                                timeInterval = Integer.parseInt(textInputTimeInterval?.text.toString())
                            } else
                                timeInterval = 300


                            Log.d("devadd", "timeint $timeInterval")
                            //write it to device
                            writeCharateristics(mAppKey!!,application.euis[0], timeInterval)
                        }

                        mBluetoothGatt.disconnect();
                        val bundle = Bundle()
                        bundle.putSerializable(APP, application)
                        activity?.runOnUiThread{
                            Toast.makeText(activity,"Device Successfully Added",Toast.LENGTH_LONG).show()
                            NavHostFragment.findNavController(this).
                                    navigate(R.id.action_nav_deviceRegistrationFragment_to_nav_appDetailsFragment, bundle)
                        }

                    }catch (e: AlreadyExistsException){
                        activity?.runOnUiThread{
                            Toast.makeText(activity,"Device with the DevEUI already exist for this application", Toast.LENGTH_LONG).show()

                        }
                    }

                }catch (e: StatusRuntimeException){
                    Log.d("ttn", e.message)
                    activity!!.runOnUiThread {
                        textInputLayoutDevId!!.error = "DevID not valid: has wrong format. IDs can contain lowercase letters, numbers, dashes and underscores and should have a maximum length of 36"
                    }

                }

            }




    }


}