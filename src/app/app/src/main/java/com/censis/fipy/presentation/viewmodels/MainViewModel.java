package com.censis.fipy.presentation.viewmodels;

import android.app.Application;

import com.censis.fipy.model.DeviceRepository;
import com.censis.fipy.model.FipyDevice;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class MainViewModel extends AndroidViewModel {

    private DeviceRepository repository;

    private MutableLiveData<FipyDevice> selected = new MutableLiveData<>();


    public MainViewModel(@NonNull Application application) {
        super(application);
        repository = DeviceRepository.getInstance();
    }


    public MutableLiveData<List<FipyDevice>> getAllFipyDevices() {
        return repository.getMutableLiveData();
    }

    public void select(FipyDevice device){
        selected.postValue(device);
    }

    public LiveData<FipyDevice> getSelectedDevice() {
        return selected;
    }




}
