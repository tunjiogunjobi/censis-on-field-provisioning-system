package com.censis.fipy.presentation.ui.fragments.kotlin

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.censis.fipy.R
import com.censis.fipy.presentation.ui.activities.kotlin.MainActivity
import org.jetbrains.anko.doAsync
import com.censis.fipy.network.api.thethingsnetwork.Service as TtnService
import kotlin.Exception

class AuthenticationFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        return inflater.inflate(com.censis.fipy.R.layout.fragment_authentication,
                container,
                false)
    }

    override fun onResume() {
        super.onResume()

        val activity = activity as MainActivity

        activity.apiService?.let{
            val intent = activity.intent

            if (intent.action == "android.intent.action.VIEW") {
                doAsync {
                    intent.data?.let { uri ->
                        if (uri.host == "oauth" && uri.scheme == "com.censis.fipy") {
                            uri.getQueryParameter("code")?.let { code ->
                                it.authenticate(code)
                            }

                            showOrGoToApps(it.isAuthenticated)
                        }
                    }
                }
            } else
                showOrGoToApps(it.isAuthenticated)
        }

        if(activity.apiService == null) {
            Log.d("show", "showing stuff now")
            val loading = view!!.findViewById(R.id.progressBar2) as ProgressBar
            val content = view!!.findViewById(R.id.content) as LinearLayout
            loading.visibility = View.GONE
            content.visibility = View.VISIBLE
        }
    }

    private fun showOrGoToApps(isAuthenticated: Boolean) {
        val activity = activity as MainActivity

        if(isAuthenticated) {
            NavHostFragment.findNavController(this)
                    .navigate(AuthenticationFragmentDirections.ActionAuthenticationFragmentToTtnApplicationsFragment())
        } else {
            activity.removeApiService()
            val loading = view!!.findViewById(R.id.progressBar2) as ProgressBar
            val content = view!!.findViewById(R.id.content) as LinearLayout
            activity!!.runOnUiThread {
                loading.visibility = View.GONE
                content.visibility = View.VISIBLE
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val mainActivity = activity as MainActivity
        mainActivity.setTitle(R.string.app_name)
        mainActivity.supportActionBar!!.setDisplayUseLogoEnabled(true)
        mainActivity.supportActionBar!!.setDisplayHomeAsUpEnabled(false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val ttnButton = view.findViewById(R.id.ttnButton) as ImageButton
        ttnButton.setOnClickListener(TtnButtonOnClick(this))
    }

    class TtnButtonOnClick(private val ref: Fragment) : View.OnClickListener {
        override fun onClick(v: View?) {
            val activity = ref.activity as MainActivity

            activity.apiService?.let {
                throw Exception("user already logged in")
            }

            val loading = ref.view!!.findViewById(R.id.progressBar2) as ProgressBar
            val content = ref.view!!.findViewById(R.id.content) as LinearLayout
            loading.visibility = View.VISIBLE
            content.visibility = View.GONE

            doAsync {
                activity.setApiService(TtnService.serviceName)

                val authUri = activity.apiService!!
                        .buildAuthenticationUri(MainActivity.OAUTH2_REDIRECT_URI)

                val browserIntent = Intent(Intent.ACTION_VIEW, authUri)
                ref.startActivity(browserIntent)
            }
        }
    }
}