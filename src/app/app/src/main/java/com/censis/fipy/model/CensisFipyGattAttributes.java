package com.censis.fipy.model;

import java.util.HashMap;

public class CensisFipyGattAttributes {
    private static HashMap<String,String> attributes = new HashMap();

    public static String CENSIS_BLE_BASE_UUID = "86f70000-a770-40ee-a66f-03aeaee5910e";
    public static String PROVISIONING_DEVICE_NAME = "CENSIS FiPy";
    public static String PROVISIONING_SERVICE_UUID = "86f7aa01-a770-40ee-a66f-03aeaee5910e";
    public static String TIME_INTERVAL_UUID = "86f7aa10-a770-40ee-a66f-03aeaee5910e";
    public static String APP_KEY_UUID = "86f7aa11-a770-40ee-a66f-03aeaee5910e";
    public static String DEV_EUI_UUID = "86f7aa12-a770-40ee-a66f-03aeaee5910e";
    public static String APP_EUI_UUID = "86f7aa13-a770-40ee-a66f-03aeaee5910e";


    private final static String DEVICE_NAME = "Device Name";
    private final static String DEV_EUI_CHARACTERISTICS = "Dev Eui Characteristics";
    private final static String TIME_INTERVAL_CHARACTERISTICS = "Time Interval Characteristics";
    private final static String PROVISIONING_SERVICE = "Provisioning Service";
    private final static String APP_KEY_CHARACTERISTICS = "App Key Characteristics";
    private final static String BASE_UUID = "Censis Base UUID";
    private final static String APP_EUI_CHARACTERISTICS = "App EUI Characteristics";




    static {
        //baseUUID
        attributes.put(BASE_UUID,CENSIS_BLE_BASE_UUID);
        // Services.
        attributes.put(PROVISIONING_SERVICE_UUID, PROVISIONING_SERVICE);
        // Characteristics.
        attributes.put(APP_KEY_UUID, APP_KEY_CHARACTERISTICS);
        attributes.put(DEV_EUI_UUID, DEV_EUI_CHARACTERISTICS);
        attributes.put(PROVISIONING_DEVICE_NAME, DEVICE_NAME);
        attributes.put(TIME_INTERVAL_UUID, TIME_INTERVAL_CHARACTERISTICS);
        attributes.put(APP_EUI_UUID,APP_EUI_CHARACTERISTICS);

    }

    public static String lookup(String uuid, String defaultName) {
        String name = attributes.get(uuid);
        return name == null ? defaultName : name;
    }
}
