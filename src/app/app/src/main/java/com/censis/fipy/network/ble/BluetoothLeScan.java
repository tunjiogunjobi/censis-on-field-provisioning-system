package com.censis.fipy.network.ble;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.os.AsyncTask;
import android.os.ParcelUuid;
import android.util.Log;

import com.censis.fipy.model.CensisFipyGattAttributes;
import com.censis.fipy.model.Device;
import com.censis.fipy.model.DeviceRepository;
import com.censis.fipy.model.FipyDevice;
import com.censis.fipy.network.api.thethingsnetwork.applications.Models;
import com.censis.fipy.presentation.ui.activities.kotlin.MainActivity;
import com.neovisionaries.bluetooth.ble.advertising.ADPayloadParser;
import com.neovisionaries.bluetooth.ble.advertising.ADStructure;
import com.neovisionaries.bluetooth.ble.advertising.Flags;
import com.neovisionaries.bluetooth.ble.advertising.ServiceData;
import com.neovisionaries.bluetooth.ble.advertising.TxPowerLevel;
import com.neovisionaries.bluetooth.ble.advertising.UUIDs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import androidx.lifecycle.MutableLiveData;

public class BluetoothLeScan  implements BleScanner {
    private BluetoothLeScanner btScanner;
    private MutableLiveData<List<FipyDevice>> liveData;

    private  List<FipyDevice> devices;

    private DeviceRepository repository;

    private Context mContext;


    public BluetoothLeScan(Context context)  {
        this.btScanner = ((BluetoothManager)context.getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter().getBluetoothLeScanner();
        devices = new ArrayList<>();
        repository = DeviceRepository.getInstance();
        liveData = repository.getMutableLiveData();
        mContext = context;
    }



    // Device scan callback.
    private ScanCallback leScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            String deviceName = null;
            String deviceAddress = null;
            String serviceUUID = null;
            if(result != null){
                Log.d("Censis", result.toString());
                BluetoothDevice bthDevice = result.getDevice();
                if(bthDevice != null){
                    deviceName = bthDevice.getName();
                    deviceAddress = bthDevice.getAddress();
                }

                List<UUID> uuidList;
                String strTxPower;
                boolean flagIsGeneralDiscoverable = false;

                ScanRecord scanRecord = result.getScanRecord();
                if (scanRecord != null)Log.d("Cencis",scanRecord.toString());
                if(scanRecord != null) {

                    // Parse the payload of the advertising packet.
                    List<ADStructure> structures = ADPayloadParser.getInstance().parse(scanRecord.getBytes());
                    UUIDs adstructureUuids;
                    Flags flags;
                    TxPowerLevel txPowerLevel;
                    ServiceData serviceData;

                    if (structures != null) {
                        for (ADStructure adStructure : structures) {
                            if (adStructure instanceof UUIDs) {
                                adstructureUuids = (UUIDs) adStructure;
                                uuidList = Arrays.asList(adstructureUuids.getUUIDs());
                                if (!uuidList.isEmpty())
                                    serviceUUID = uuidList.get(0).toString();
                                Log.d("Adstructure", adstructureUuids.toString());

                            } else if (adStructure instanceof Flags) {
                                flags = (Flags) adStructure;
                                flagIsGeneralDiscoverable = flags.isGeneralDiscoverable();
                                Log.d("Adstructure", flags.toString());

                            } else if (adStructure instanceof TxPowerLevel) {
                                txPowerLevel = (TxPowerLevel) adStructure;
                                strTxPower = Integer.toString(txPowerLevel.getLevel()) + "dBm";
                                Log.d("Adstructure", strTxPower);
                            } else if (adStructure instanceof ServiceData) {
                                serviceData = (ServiceData) adStructure;
                                Log.d("Adstructure", serviceData.getServiceUUID().toString());

                            }
                        }
                    }

                }


                FipyDevice device = new FipyDevice(deviceName,deviceAddress);


                if(!devices.contains(device)){
                    devices.add(device);
                    liveData.postValue(devices);

                    if(mContext != null){
                        MainActivity activity = (MainActivity) mContext;
                        activity.setTitle("Available Devices");
                    }
                }
            }

        }
    };


    public void startScanning() {

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                ScanSettings scanSettings = new ScanSettings.Builder().
                        setScanMode(ScanSettings.SCAN_MODE_LOW_POWER).build();
                ScanFilter serviceUuidFilter = new ScanFilter.Builder().
                        setServiceUuid(new ParcelUuid(UUID.fromString(CensisFipyGattAttributes.PROVISIONING_SERVICE_UUID))).build();
                List<ScanFilter> filters = new ArrayList<>();
                filters.add(serviceUuidFilter);
                if(btScanner != null)
                    btScanner.startScan(filters,scanSettings,leScanCallback);
            }
        });
    }

    public void stopScanning() {

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                btScanner.stopScan(leScanCallback);
            }
        });
    }
    public void clear(){
        devices.clear();
        liveData.postValue(devices);
    }}
