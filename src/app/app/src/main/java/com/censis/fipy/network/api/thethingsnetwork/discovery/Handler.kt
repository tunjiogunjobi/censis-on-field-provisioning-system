package com.censis.fipy.network.api.thethingsnetwork.discovery

import android.app.Activity
import android.content.Context
import android.util.Log
import com.censis.fipy.Utils
import io.grpc.ManagedChannel
import io.grpc.StatusRuntimeException
import io.grpc.okhttp.OkHttpChannelBuilder
import org.thethingsnetwork.api.discovery.DiscoveryGrpc
import org.thethingsnetwork.api.discovery.GetRequest
import java.time.Instant
import java.util.concurrent.TimeUnit

class Handler(private val activity: Activity) {

    companion object {
        const val DISCOVERY_URI = "discovery.thethingsnetwork.org"
        const val DISCOVERY_PORT = 1900
        const val CACHE_EXPIRES = 604800L
    }

    private lateinit var managedChannel: ManagedChannel
    private lateinit var blockingStub: DiscoveryGrpc.DiscoveryBlockingStub

    fun shutdown() {
        managedChannel.shutdownNow().awaitTermination(5, TimeUnit.SECONDS)
    }

    fun getHandler(handler: String): Announcement {
        val prefs = activity.getPreferences(Context.MODE_PRIVATE)
        val expiration = prefs.getLong("handler_${handler}_cache_expiration", 0)
        val handlerObject = prefs.getString("handler_${handler}_cache", "")

        if(expiration != 0L && expiration > Instant.now().epochSecond && handlerObject.isNotEmpty()) {
            val announcement = handlerObject?.let { Utils.unserializeObject<Announcement>(it) }
            if(announcement != null) {
                return announcement
            }
        }

        managedChannel = OkHttpChannelBuilder.forAddress(DISCOVERY_URI, DISCOVERY_PORT)
                .useTransportSecurity()
                .build()
        blockingStub = DiscoveryGrpc.newBlockingStub(managedChannel)

        val req = GetRequest.newBuilder()
                .setServiceName("handler")
                .setId(handler)
                .build()

        try {
            val app = Announcement(blockingStub.get(req))

            prefs.edit()
                    .putString("handler_${handler}_cache", Utils.serializeObject(app))
                    .putLong("handler_${handler}_cache_expiration", Instant.now().epochSecond + CACHE_EXPIRES)
                    .apply()

            return app
        } catch(e: StatusRuntimeException) {
            Log.e("ttn.discovery", "getHandler error", e)
            throw Exception("invalid ttn handler name")
        }
    }
}