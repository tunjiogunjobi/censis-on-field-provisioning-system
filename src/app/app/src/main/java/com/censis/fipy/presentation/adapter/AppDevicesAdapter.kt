package com.censis.fipy.presentation.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.censis.fipy.R
import com.censis.fipy.Utils

import com.censis.fipy.model.Application
import com.censis.fipy.model.Device

class AppDevicesAdapter(private val app: Application) : RecyclerView.Adapter<AppDevicesAdapter.CustomViewHolder>() {
    private var mDevices: MutableList<Device>? = null

    class CustomViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var mTextviewDevEui: TextView = view.findViewById(R.id.textView_device_address)
        var mTextViewDevId: TextView = view.findViewById(R.id.textView_device_name)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_item, parent, false)

        return CustomViewHolder(view)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        if (mDevices != null && !mDevices!!.isEmpty()) {
            val device = mDevices!![position]

            var id = device.devId
            val devEui = device.devEui

            holder.mTextViewDevId.text = id

            holder.mTextviewDevEui.text = Utils.bytesToHex(devEui)

            val view = holder.mTextViewDevId.rootView

            view.setOnClickListener {
                val bundle = Bundle()
                bundle.putSerializable("app", app)
                bundle.putString("devId", id)
                Navigation.findNavController(view).navigate(R.id.action_nav_appDetailsFragment_to_deviceDetailsFragment, bundle)
            }
        } else {
            // Covers the case of data not being ready yet.
            holder.mTextViewDevId.setText(R.string.no_devices)
        }

//        val view = holder.mTextViewDevId.getRootView()
//        view.setOnClickListener({ v ->
//            val bundle:Bundle = Bundle()
//            bundle.putSerializable("fipyDevice", mDevices?.get(position))
//            Navigation.findNavController(view).navigate(R.id.action_nav_appDetailsFragment_to_deviceDetailsFragment, bundle)
//        })
    }

    override fun getItemCount(): Int {
        return if (mDevices != null)
            mDevices!!.size
        else
            0

    }

    fun setDevices(devices: MutableList<Device>) {
        mDevices = devices
        notifyDataSetChanged()
    }

    internal fun clear() {
        if (mDevices != null)
            mDevices!!.clear()
    }
}
