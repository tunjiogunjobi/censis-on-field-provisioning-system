package com.censis.fipy.network.ble;

import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import java.util.List;

import androidx.lifecycle.MutableLiveData;

import static android.content.Context.BIND_AUTO_CREATE;

public class FipyBleServiceConnection extends BroadcastReceiver implements ServiceConnection {

    private BluetoothLeService mBluetoothLeService;
    private final String TAG = FipyBleServiceConnection.class.getSimpleName();
    private boolean mConnected = false;
    private Context mContext;
    private List<BluetoothGattService> mGattServices;
    private MutableLiveData<List<BluetoothGattService>> mLiveData;

    private MutableLiveData<String> mLiveDataField;



    public FipyBleServiceConnection(Context context) {
        mContext = context;
        mLiveData = new MutableLiveData<>();
        mLiveDataField = new MutableLiveData<>();
        Intent gattServiceIntent = new Intent(context, BluetoothLeService.class);
        context.bindService(gattServiceIntent, this, BIND_AUTO_CREATE);
    }

    public void unBindBleService(){
        try {
            mContext.unbindService(this);
            mBluetoothLeService = null;
        }catch (IllegalArgumentException e){
            Log.d(FipyBleServiceConnection.class.getSimpleName(),"Service Not Registered");
        }

    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
        if (!mBluetoothLeService.initialize()) {
            Log.e(TAG, "Unable to initialize Bluetooth");
            //finish(); TODO
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mBluetoothLeService = null;

    }


    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
            mConnected = true;
            //updateConnectionState(R.string.connected);
            //getActivity().invalidateOptionsMenu();
        } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
            mConnected = false;
            //updateConnectionState(R.string.disconnected);
            //getActivity().invalidateOptionsMenu();
            clear();
        } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
            // Show all the supported services and characteristics on the user interface.
            // Add supported services and characteristics to list
            mGattServices = mBluetoothLeService.getSupportedGattServices();
            mLiveData.postValue(mGattServices);
            //displayGattServices(mBluetoothLeService.getSupportedGattServices());
        } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
            //displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
            mLiveDataField.postValue(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
        }
    }

    public boolean connectToDevice(String deviceAddress){
        Log.d(FipyBleServiceConnection.class.getSimpleName(),"connectToDeviceCalled");
        //connects to the device upon successful start-up initialization.
        if(mBluetoothLeService != null)
            return mBluetoothLeService.connect(deviceAddress);
        return false;
    }

    public void disconnect(){
        mBluetoothLeService.disconnect();
    }


    public boolean isConnected(){
        return mConnected;
    }

    public void clear(){
        if (mGattServices != null){
        mGattServices.clear();
        mLiveData.postValue(mGattServices);}
    }

    public MutableLiveData<List<BluetoothGattService>> getGattServices(){

        return mLiveData;
    }

    public MutableLiveData<String> getData(){
        return mLiveDataField;
    }

}


