package com.censis.fipy.network.api

class AlreadyExistsException : Exception {
    constructor(message: String) : super(message)
    constructor(): super()
}