package com.censis.fipy.model;

import java.io.Serializable

data class FipyDevice(var dev_id: String?, val bleMacAddress: String?, var serviceUUID
            : String?, var lorawanDevice: Device?):Serializable {

    constructor(deviceName: String, deviceAddress: String) : this(deviceName,deviceAddress,null,null) {

    }


    var gattServices: MutableList<String> = ArrayList()

    var altitude: Int? = null
    var app_id: String? = null

}

