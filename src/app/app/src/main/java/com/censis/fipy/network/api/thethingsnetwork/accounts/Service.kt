package com.censis.fipy.network.api.thethingsnetwork.accounts

import retrofit2.Call
import retrofit2.http.*

internal interface Service {
    @Headers("Content-Type: application/json")
    @POST("/users/token")
    fun getBearerToken(
            @Header("Authorization") authorization: String,
            @Body body: Models.TokenRequest
    ): Call<Models.TokenData>

    @Headers("Content-Type: application/json")
    @POST("/users/token")
    fun refreshBearerToken(
            @Header("Authorization") authorization: String,
            @Body body: Models.TokenRefreshRequest
    ): Call<Models.TokenData>

    @Headers("Content-Type: application/json")
    @POST("/users/restrict-token")
    fun exchangeToken(
            @Header("Authorization") bearerToken: String,
            @Body body: Models.ExchangeTokenRequest
    ): Call<Models.TokenData>

    @GET("/applications")
    fun getApplications(
            @Header("Authorization") authorization: String
    ): Call<List<Models.ApplicationData>>

    @GET("/applications/{app_id}")
    fun getApplication(
            @Header("Authorization") authorization: String,
            @Path("app_id") appId: String
    ): Call<Models.ApplicationData>
}