package com.censis.fipy.network.api

import android.app.Activity
import com.censis.fipy.network.api.thethingsnetwork.Service as TtnService

object Factory {

    private const val TTN_HANDLER = "ttn-handler-eu"
    private const val TTN_CLIENT_ID = "censis-fipy-app-editor"
    private const val TTN_CLIENT_SECRET = "1kIyuU7cwppzzoAshKLPhGaQUfIcRffOO5BlP46bB00lUsV5WXsMM4v4"

    @Throws(IllegalArgumentException::class)
    fun makeService(activity: Activity, serviceName: String, vararg extra: String): Service {
        when (serviceName) {
            TtnService.serviceName -> return if (extra.isEmpty())
                TtnService(activity, TTN_HANDLER, TTN_CLIENT_ID, TTN_CLIENT_SECRET)
            else
                throw IllegalArgumentException("invalid arguments.")
        }

        throw IllegalArgumentException("the API service $serviceName is not implemented.")
    }
}
