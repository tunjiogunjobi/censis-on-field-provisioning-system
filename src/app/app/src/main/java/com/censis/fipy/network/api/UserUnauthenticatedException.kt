package com.censis.fipy.network.api

class UserUnauthenticatedException : Exception {
    constructor(message: String) : super(message)
    constructor(): super()
}