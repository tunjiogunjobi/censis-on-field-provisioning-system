package com.censis.fipy.presentation.ui.fragments.kotlin

import android.graphics.Typeface
import android.opengl.Visibility
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.censis.fipy.presentation.ui.activities.kotlin.MainActivity
import com.censis.fipy.R
import com.censis.fipy.Utils
import com.censis.fipy.model.Application
import com.censis.fipy.model.Device
import com.censis.fipy.model.FipyDevice
import com.censis.fipy.network.api.UserUnauthenticatedException
import com.censis.fipy.presentation.adapter.AppDevicesAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson
import org.jetbrains.anko.toast

class AppDetailsFragment:Fragment() {
    lateinit var app: Application

    private lateinit var appNameTextView: TextView
    private lateinit var appIdTextView: TextView
    private lateinit var appEuiTextView: TextView

    private val APP:String = "app"

    var mDevices: List<Device> = mutableListOf()

    private lateinit var mListAdapter: AppDevicesAdapter
    lateinit var recyclerView: RecyclerView
    private lateinit var container: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            app = it.getSerializable(APP) as Application
        }
        setHasOptionsMenu(true)
        mListAdapter = AppDevicesAdapter(app)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_app_details, container,false)
        return view //super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appNameTextView = view.findViewById(R.id.textView_app_name)
        appEuiTextView = view.findViewById(R.id.textView_app_eui)

        if(app.name.isEmpty()) {
            appNameTextView.setTypeface(appNameTextView.typeface, Typeface.ITALIC)
            appNameTextView.text = "Unknown"
        } else
            appNameTextView.text = app.name
        appEuiTextView.text = app.euis.joinToString("\n") { Utils.bytesToHex(it) }

        container = view.findViewById(R.id.pullToRefreshApp)
        container.setOnRefreshListener {
            loadDevices()
        }

        recyclerView = container.findViewById(R.id.recyclerview_devices)
        recyclerView.adapter = mListAdapter
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.visibility = View.GONE

        view.findViewById<FloatingActionButton>(R.id.floatingActionButton)
                .setOnClickListener {
                    val bundle = Bundle()
                    bundle.putSerializable(APP, app)
                    NavHostFragment.findNavController(this)
                            .navigate(R.id.action_nav_appDetailsFragment_to_nav_scan_device_fragment, bundle)
                }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val mainActivity = activity as MainActivity
        mainActivity.title = app.id
        mainActivity.supportActionBar!!.setDisplayUseLogoEnabled(false)
        mainActivity.supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        loadDevices()
    }

    private fun loadDevices() {
        val mainActivity = activity as MainActivity

        container.isRefreshing = true
        view?.findViewById<TextView>(R.id.devicesError)?.visibility = View.GONE
        AsyncTask.execute {
            try {
                //TODO the line below foreclosing Invalid argument error
                mainActivity.apiService?.getDevices(app)?.let {
                    mDevices = it

                }

                activity?.runOnUiThread {
                    mListAdapter.setDevices(mDevices.toMutableList())
                    Log.d("ttn", "mDevice size is now: " + mDevices.size )

                    if(mDevices.isEmpty())
                        view?.findViewById<TextView>(R.id.noDevicesView)?.visibility = View.VISIBLE
                    else
                        recyclerView.visibility = View.VISIBLE
                }
            } catch(e: UserUnauthenticatedException) {
                activity?.runOnUiThread {
                    context?.toast(R.string.user_unauthenticated)
                }
                mainActivity.resetAndGoToLogin(NavHostFragment.findNavController(this))
                TODO("recover on timeout or no network")
            } catch(e: Exception) {
                activity?.runOnUiThread {
                    view?.findViewById<TextView>(R.id.noDevicesView)?.visibility = View.GONE
                    view?.findViewById<TextView>(R.id.devicesError)?.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                }
            }

            activity?.runOnUiThread {
                container.isRefreshing = false
            }
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                NavHostFragment.findNavController(this).navigateUp()
                return true
            }
        }

        return false
    }
}