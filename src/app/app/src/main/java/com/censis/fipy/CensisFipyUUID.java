package com.censis.fipy;

public class CensisFipyUUID {

    public static final String TAG = "cencisFipy";
    public static final String CENSIS_BLE_BASE_UUID = "86f70000-a770-40ee-a66f-03aeaee5910e";
    public static final String PROVISIONING_DEVICE_NAME = "CenPy";
    public static final String PROVISIONING_SERVICE_UUID = "86f7aa01-a770-40ee-a66f-03aeaee5910e";
    public static final String TIME_INTERVAL_UUID = "86f7aa10-a770-40ee-a66f-03aeaee5910e";
    public static final String APP_KEY_UUID = "86f7aa11-a770-40ee-a66f-03aeaee5910e";
    public static final String DEV_EUI_UUID = "86f7aa12-a770-40ee-a66f-03aeaee5910e";
    public static final String APP_EUI_UUID = "86f7aa13-a770-40ee-a66f-03aeaee5910e";

}
