"""
Helper module to expose configuration parameters
for reading and/or writing using BLE.
"""
from network import Bluetooth
import binascii
import config
import time
import _thread

READ_ONLY = Bluetooth.PROP_READ
WRITE_ONLY = Bluetooth.PROP_WRITE
NOTIFY_PROP = Bluetooth.PROP_NOTIFY
ALL_PROPS = READ_ONLY | WRITE_ONLY | NOTIFY_PROP
RW_PROP = READ_ONLY | WRITE_ONLY

_READ_EVENT = Bluetooth.CHAR_READ_EVENT
_WRITE_EVENT = Bluetooth.CHAR_WRITE_EVENT
_RW_EVENT = _READ_EVENT | _WRITE_EVENT

CAST_AS_INT = 1
CAST_AS_BOOLEAN = 2
CAST_AS_ASCII = 3
CAST_AS_B64 = 4

_bluetooth = Bluetooth()

_service = None
_characteristics = {}
_base_uuid = None
_is_advertising = False
_is_provisioning = False

def uuid_to_bytes(uuid: str) -> bytes:
    """
    Turn a 128-bit UUID string into bytes
    """
    plain_uuid = uuid.replace("-", "")

    if len(plain_uuid) % 2 != 0:
        raise ValueError('malformed UUID')

    reversed_uuid = []

    for idx in reversed(range(0, len(plain_uuid)-1, 2)):
        reversed_uuid.append(plain_uuid[idx] + plain_uuid[idx+1])

    return binascii.unhexlify("".join(reversed_uuid))

def uuid_offset(offset: int) -> bytes:
    """
    Sets a 16-bit UUID on a 128-bit base vendor UUID
    """
    if type(_base_uuid) is not bytes:
        raise Exception("Provisioner not set up yet")
    if offset < 0 or offset > 0xffff:
        raise ValueError("The offset must be a 16-bit unsigned integer")

    base_uuid = bytearray(_base_uuid)
    base_uuid[12] = offset & 0xff
    base_uuid[13] = offset >> 8

    return bytes(base_uuid)


def setup(device_name: str, base_uuid: str, service_uuid: int, settings_number: int):
    """
    Sets up the advertisement and the provisioning
    service, ready to advertise.

    Attributes
    ----------
    device_name: str
        name of the device for the BLE advertisement
    base_uuid: str
        the base 128-bit UUID for the vendor
    service_uuid: int
        16-bit UUID for the service
    settings_number: int
        Total number of settings that can be exposed
    """
    global _service, _base_uuid

    if settings_number < 1:
        raise ValueError("The total number of settings is not of a valid value")

    _base_uuid = uuid_to_bytes(base_uuid)

    service_uuid = uuid_offset(service_uuid)

    def conn_cb(bt_o):
        global _is_provisioning

        events = bt_o.events()
        if events & Bluetooth.CLIENT_CONNECTED:
            print("Provisioning...")
            _is_provisioning = True
            stop()
        elif events & Bluetooth.CLIENT_DISCONNECTED:
            print("Provisioning ended")
            _is_provisioning = False

    _bluetooth.callback(trigger=Bluetooth.CLIENT_CONNECTED | Bluetooth.CLIENT_DISCONNECTED, handler=conn_cb)

    _bluetooth.set_advertisement(name=device_name[:5], service_uuid=service_uuid)
    _service = _bluetooth.service(
        uuid=service_uuid,
        isprimary=True,
        nbr_chars=settings_number
    )

def start():
    """
    Start advertising the device
    """
    global _is_advertising
    if _is_advertising is False:
        _bluetooth.advertise(True)
        _is_advertising = True
        print("Advertising...")

def stop():
    """
    Stop advertising the device
    """
    global _is_advertising
    if _is_advertising is True:
        _bluetooth.advertise(False)
        _is_advertising = False
        print("Advertisement ended")

def expose(
    setting_key: str,
    characteristic_uuid: int,
    cast_as=None,
    properties=ALL_PROPS,
    fetch_value=None,
    set_value=None
):
    """
    Expose a setting as a BLE characteristic

    Attributes
    ----------
    setting_key: str
        Key for the setting to expose
    characteristic_uuid: int
        16-bit UUID for the setting-related characteristic
    properties: int
        The characteristic properties: read, write and/or notify.
        Default: read, write and notify.
    fetch_value
        Function to retrieve the setting value
    set_value
        Function to set the setting value
    """
    if _service == None:
        raise Exception("Provisioner not set up yet")

    if setting_key in _characteristics:
        raise Exception("There is already a characteristic with this UUID")

    if not callable(fetch_value):
        if cast_as == CAST_AS_B64:
            fetch_value = lambda k: config.get_b64(k)
        else:
            fetch_value = lambda k: config.get(k)

    if not callable(set_value):
        if cast_as == CAST_AS_B64:
            set_value = lambda k, v: config.set_b64(k, v)
        else:
            set_value = lambda k, v: config.set(k, v)

    _characteristics[setting_key] = _service.characteristic(
        uuid=uuid_offset(characteristic_uuid),
        properties=properties,
        value=fetch_value(setting_key)
    )

    def common_handler(char):
        events = char.events()

        if events & _WRITE_EVENT:
            value = char.value()

            try:
                if cast_as == CAST_AS_INT:
                    value = int.from_bytes(value, 'big')
                elif cast_as == CAST_AS_BOOLEAN:
                    value = bool(int.from_bytes(value, 'big') & 1)
                elif cast_as == CAST_AS_ASCII:
                    value = value.decode("ascii")

                set_value(setting_key, value)
            except:
                pass
        else:
            return char.value()


    _characteristics[setting_key].callback(
        trigger=_RW_EVENT,
        handler=common_handler
    )

def is_provisioning():
    return _is_provisioning

def is_advertising():
    return _is_advertising

def run(advertisement_duration: int):
    """
    Run provisioner for the amount of advertisement_duration
    milliseconds, or if a device is connected, till it
    disconnects.

    Attributes
    ----------
    advertisement_duration: int
        Duration of the BLE advertisement in milliseconds.
    """
    if advertisement_duration < 0:
        raise ValueError("The advertisement duration is invalid.")

    start()

    def wait_and_stop(duration):
        time.sleep_ms(duration)
        stop()

    _thread.start_new_thread(wait_and_stop, (advertisement_duration,))
