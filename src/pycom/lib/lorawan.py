# A header file designed to reduce the difficulty in use of the LoRa package,
# and to simplify its functionality and its function calls
# All methods return True to indicate they have finished without issue

from network import LoRa
import socket
import config
import time

# modes
LORA = 1
LORAWAN = 2

# regions
ASIA = "AS"
AUSTRALIA = "AU"
EUROPE = "EU"
USA = "US"

# Instantiates a LoRa instance fo use in main.py. Simplifies by reducing need to
# look up complicated region codes.
#
# Hand the function in a value in brackets, and it will use the specified mode
# and region.
# Recognised modes:             Recognised Regions:
# LoRa      (1)                 Asia = LoRa.AS923               ("AS")
# LoRaWAN   (2)                 Australia = LoRa.AU915          ("AU")
#                               Europe = LoRa.EU868             ("EU")
#                               United States = LoRa.US915      ("US")
class lorawan:
    # Initialises the port so the FiPy can send data over LoRa to the gateway server once connected
    # This needs to be redone every time the device wakes from deepsleep, so it is put in a separate
    # function from connect()
    __instance=None
    lora_obj = None

    def getInstance():
        if lorawan.__instance==None:
            lorawan()
        return lorawan.__instance

    def make_socket(self):
        s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
        s.setsockopt(socket.SOL_LORA, socket.SO_DR, 5)
        s.setsockopt(socket.SOL_LORA, socket.SO_CONFIRMED, True)
        return s

    def __init__(self,mode,region):
        self.setreg = 0
        self.setmode = 0
        self.connected = 0
        self.s = 0
        self.lora_obj = 0

        if (region=="EU"):
            setreg=LoRa.EU868
        elif (region=="AU"):
            setreg=LoRa.AU915
        elif (region=="AS"):
            setreg=LoRa.AS932
        elif (region=="US"):
            setreg=LoRa.US915
        else:
            print("Not an Accepted Region!")
            return None

        if (mode==1):
            setmode=LoRa.LORA
        elif (mode==2):
            setmode=LoRa.LORAWAN
        else:
            print("Not an Acceptable Mode!")
            return None

        self.lora_obj = LoRa(mode=setmode,region=setreg)
        if self.lora_obj!=0:
            print("lora_obj was correctly set to non-0 value")

        self.s = self.make_socket()

    # Connects the FiPy to the LoRa gateway with the given credentials,
    # and initialises the socket so it can communitcate.
    # Activation can be either OTAA or
    def activate(self,activation):
        # restore any previous session keys
        self.load()

        # Causes the FiPy to initialise the LoRa connection, and retry until it is
        # successful in connecting
        if not self.lora_obj.has_joined():
            if (activation=="OTAA"):
                activation = LoRa.OTAA
            elif (activation=="ABP"):
                activation = LoRa.ABP
            else:
                print ("Not an acceptable Authentication method!")
                return None

            # checking if app key and eui are correctly set
            app_key, app_eui = config.get_app_key(), config.get_app_eui()
            if app_key == None or app_eui == None:
                print ("Either the app key or the app eui have not been set yet.")
                return None

            self.lora_obj.join(activation=activation,auth=(app_eui, app_key) , timeout=0)
            
            while not self.lora_obj.has_joined():
                time.sleep(2.5)
                print('Not yet joined...')

        return True

    # Function for sending data via the LoRa connection. takes the data, and encodes it into ASCII
    # The send function automatically
    def send(self,data,max_attempts=10):
        if type(data) is not str or type(data) is not bytes:
            data = str(data)

        self.s.setblocking(True)

        successfully_sent = False
        current_attempt = 0
        while not successfully_sent and current_attempt < max_attempts:
            try:
                self.s.send(data)
                successfully_sent = True
            except:
                pass

            current_attempt += 1

        self.s.setblocking(False)

        try:
            rssi = self.lora_obj.stats().rssi
        except:
            rssi = 0
        config.set("rssi", rssi)
        print("stats:", self.lora_obj.stats(), "rssi:", rssi, "config.rssi", config.get("rssi"))

        return True

    # Saves the current LoRa status
    def save(self):
        self.lora_obj.nvram_save()
        return True

    # Reloads the saved LoRa status
    def load(self):
        self.lora_obj.nvram_restore()
        return True
