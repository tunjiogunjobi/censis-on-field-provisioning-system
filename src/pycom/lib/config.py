import json
import binascii

_CONFIG = None
_CONFIG_FILE_PATH = None

def load(config_file_path: str) -> bool:
    global _CONFIG, _CONFIG_FILE_PATH
    f = None
    loaded_successfully = False

    try:
        f = open(config_file_path)
        _CONFIG = json.loads("".join(f.readlines()))
        loaded_successfully = True
    except:
        _CONFIG = {}
    finally:
        if f != None:
            f.close()
        _CONFIG_FILE_PATH = config_file_path

    return loaded_successfully

def get(key: str) -> str:
    if key in _CONFIG:
        return _CONFIG[key]

    return None

def set(key: str, value: str):
    global _CONFIG
    _CONFIG[key] = value

    try:
        f = open(_CONFIG_FILE_PATH, "w")
        f.write(json.dumps(_CONFIG))
        f.close()
    except Exception as e:
        print(e)

def has(key: str) -> bool:
    if key in _CONFIG:
        return True
    else:
        return False

def get_b64(key: str) -> bytes:
    try:
        entry = get(key)
        if entry:
            return binascii.a2b_base64(entry)
    except:
        pass

    return None

def set_b64(key: str, value: bytes):
    set(key, binascii.b2a_base64(value)[:-1].decode("ascii"))

def get_app_key() -> bytes:
    return get_b64('app_key')

def set_app_key(value: bytes):
    set_b64("app_key", value)

def get_app_eui() -> bytes:
    return get_b64('app_eui')

def set_app_eui(value: bytes):
    set_b64("app_eui", value)
