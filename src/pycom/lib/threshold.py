class threshold:

    def __init__(self, lowBound:int =-100000, upBound:int =100000):
        self.lowBound = lowBound
        self.upBound = upBound

    # Checks if the given value is within the given boundaries. The boundaries are inclusive here
    def within_boundary(self,compVal:int):
        if ((compVal <= self.upBound and compVal >= self.lowBound)):
            return True
        else:
            return False

    # Checks if the given value is outside the given boundaries. The boundaries are exclusive here
    def outside_boundary(self,compVal:int):
        if (compVal > self.upBound or compVal < self.lowBound):
            return True
        else:
            return False

    def equals_upper_bound(self, compVal:int):
        if (compVal == self.upBound):
            return True
        else:
            return False

    def equals_lower_bound(self,compVal:int):
        if (compVal == self.lowBound):
            return True
        else:
            return False

    def get_upBound(self):
        return self.upBound

    def get_lowBound(self):
        return self.lowBound
