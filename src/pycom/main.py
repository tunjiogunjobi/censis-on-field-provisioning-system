#The FiPy will automatically run boot.py on startup,
#so dont worry about including it here

import machine                      # Used for deepsleep
import config                       # Used to retrieve the provisioned data
from threshold import threshold     # Standard set of boundaries to test data against
import lorawan                      # Used for LoRa functionality
from network import LTE             # Used to turn off power-consuming LTE module each wakeup

lora = lorawan.lorawan(lorawan.LORAWAN, lorawan.EUROPE)
lora.activate("OTAA")

lte = LTE()
try:
    lte.dettach()
    lte.deinit()
except:
    pass

# The default sender_data variable, put the data you want to send to your network server
# in this variable, and it will be sent before the device goes back into deepsleep
sender_data = 12345

#   place any code you want the device
#   to execute into this space, such as
#   reading in data from a serial port
#   or performing calculations on this data

lora.send(sender_data)

# save and go to deep sleep
lora.save()
time_interval = config.get("time_interval")
if type(time_interval) is int and time_interval > 0:
    machine.deepsleep(time_interval*1000) # convert seconds to milliseconds
else:
    machine.deepsleep() # sleep indefinitely
