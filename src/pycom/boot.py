# boot.py -- run on boot-up
import pycom
from network import LTE, WLAN, LoRa
import config
import provisioner
import machine

lte = LTE()
wlan = WLAN()

#   This section initially deactivates the
#   wifi, LET heartbeat, WLAN and LTE modules
#   as theya re not used for this implementation
pycom.wifi_on_boot(False)
pycom.heartbeat(False)
try:
    lte.dettach()
    lte.deinit()
    wlan.deinit()
except:
    pass

#sets the file where this data is stored
CONFIG_FILE_PATH = "/flash/config.json"

#basic setup information
PROVISIONING_WAIT_TIME = 20000
BLE_VENDOR_BASE_UUID = "86f70000-a770-40ee-a66f-03aeaee5910e"
PROVISIONING_DEVICE_NAME = "CENSIS FiPy"
PROVISIONING_SERVICE_UUID = 0xaa01
TIME_INTERVAL_UUID = 0xaa10
APP_KEY_UUID = 0xaa11
DEV_EUI_UUID = 0xaa12
APP_EUI_UUID = 0xaa13
RSSI_UUID = 0xaa14

config.load(CONFIG_FILE_PATH)

#   makes the device sit for 'time_interval' time
#   waiting for the device to be connected to by a bluetooth device
reason, gpio = machine.wake_reason()
if reason is not machine.RTC_WAKE:
    provisioner.setup(
        device_name=PROVISIONING_DEVICE_NAME,
        base_uuid=BLE_VENDOR_BASE_UUID,
        service_uuid=PROVISIONING_SERVICE_UUID,
        settings_number=4
    )
    provisioner.expose("dev_eui", DEV_EUI_UUID,
        properties=provisioner.READ_ONLY,
        fetch_value=lambda k: LoRa().mac()
    )
    provisioner.expose("time_interval", TIME_INTERVAL_UUID,
        cast_as=provisioner.CAST_AS_INT
    )
    provisioner.expose("app_key", APP_KEY_UUID,
        properties=provisioner.WRITE_ONLY,
        cast_as=provisioner.CAST_AS_B64
    )
    provisioner.expose("app_eui", APP_EUI_UUID,
        properties=provisioner.WRITE_ONLY,
        cast_as=provisioner.CAST_AS_B64
    )
    provisioner.expose("rssi", RSSI_UUID,
        properties=provisioner.READ_ONLY,
        cast_as=provisioner.CAST_AS_INT
    )

    provisioner.run(PROVISIONING_WAIT_TIME)

    while provisioner.is_advertising() or provisioner.is_provisioning():
        continue
