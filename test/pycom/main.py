import sys
import unittest

sys.path.append('../../src/pycom/')
sys.path.append('../../src/pycom/lib')

if __name__ == "__main__":
    suite = unittest.TestLoader().discover('tests', pattern='test_*.py')
    result = unittest.TextTestRunner(verbosity=2).run(suite)
    sys.exit(not result.wasSuccessful())
