import unittest
import config
import os
import json


class ConfigTest(unittest.TestCase):
    __CONFIG_FILE_PATH = "config.json"
    __APP_KEY_TEST = b"\xaa\xbb\xcc\xdd\xee\xff\x11\x22\x33\x44\x55\x66"

    def setUp(self):
        config.load(self.__CONFIG_FILE_PATH)

    def test_app_key(self):
        config.set_app_key(self.__APP_KEY_TEST)
        self.assertEqual(config.get_app_key(), self.__APP_KEY_TEST)

    def test_if_setting(self):
        config.set("my_random_key", "my_random_value")
        self.assertTrue(config.has("my_random_key"))
        self.assertEqual(config.get("my_random_key"), "my_random_value")

    def test_file_created(self):
        config.set("my_random_key", "my_random_value")
        self.assertTrue(os.path.isfile(self.__CONFIG_FILE_PATH))

    def test_file_contents(self):
        my_config = {
            "my_random_key": "my_random_value",
            "app_key": "qrvM3e7/ESIzRFVm"
        }
                
        config.set("my_random_key", "my_random_value")
        config.set_app_key(self.__APP_KEY_TEST)

        with open(self.__CONFIG_FILE_PATH) as file:
            self.assertEqual(file.read(), json.dumps(my_config))

    def tearDown(self):
        try:
            os.remove(self.__CONFIG_FILE_PATH)
        except OSError:
            pass
