import unittest
from threshold import threshold
import os

class thresholdTest(unittest.TestCase):
    thresh = threshold(-100,100)

    def test_withinBoundary(self):
        #normal value within range
        compVal=10
        self.assertTrue(self.thresh.within_boundary(compVal))
        #values far exceeding upper or lower boundary
        compVal=200
        self.assertFalse(self.thresh.within_boundary(compVal))
        compval=-200
        self.assertFalse(self.thresh.within_boundary(compVal))
        #upper boundary testing
        compVal=100
        self.assertTrue(self.thresh.within_boundary(compVal))
        compVal=99
        self.assertTrue(self.thresh.within_boundary(compVal))
        #lower boundary testing
        compVal=-99
        self.assertTrue(self.thresh.within_boundary(compVal))
        compVal=-100
        self.assertTrue(self.thresh.within_boundary(compVal))

    def test_outsideBoundary(self):
        #values far exceeding upper or lower boundary
        compVal=200
        self.assertTrue(self.thresh.outside_boundary(compVal))
        compval=-200
        self.assertTrue(self.thresh.outside_boundary(compVal))
        #upper boundary testing
        compVal=100
        self.assertFalse(self.thresh.outside_boundary(compVal))
        compVal=101
        self.assertTrue(self.thresh.outside_boundary(compVal))
        #lower boundary testing
        compVal=-100
        self.assertFalse(self.thresh.outside_boundary(compVal))
        compVal=-101
        self.assertTrue(self.thresh.outside_boundary(compVal))

    def test_equalsUpperBound(self):
        #testing using a regular value
        compVal=100
        self.assertTrue(self.thresh.equals_upper_bound(compVal))
        #boundary testing
        compVal=99
        self.assertFalse(self.thresh.equals_upper_bound(compVal))
        compVal=101
        self.assertFalse(self.thresh.equals_upper_bound(compVal))

    def test_equalsLowerBound(self):
        #testing using a regular value
        compVal=-100
        self.assertTrue(self.thresh.equals_lower_bound(compVal))
        #boundary testing
        compVal=-99
        self.assertFalse(self.thresh.equals_lower_bound(compVal))
        compVal=-101
        self.assertFalse(self.thresh.equals_lower_bound(compVal))

    def test_Get(self):
        self.assertEqual(self.thresh.get_upBound(),100)
        self.assertEqual(self.thresh.get_lowBound(),-100)
