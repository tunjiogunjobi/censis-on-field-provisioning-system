# Contributing.md

### Project contributors
- Luca Vizzarro (2252593v@student.gla.ac.uk)
- Yusuf Ogunjobi (1009097o@student.gla.ac.uk)
- Amirul L Jamaludin (2259783j@student.gla.ac.uk)
- Cameron Taylor (2256483t@student.gla.ac.uk)

### Repository structure
- **docs/**: documentation folder
- **src/pycom**: Pycom software source code
- **src/app**: Android app source code

### Contributing guidelines
- Create an issue for each feature and bug fixes, associating the suitable labels
- Close the issue using the commit message
- Feature branch workflow: create a branch for each new feature, create a merge request and close it upon feature development completion
